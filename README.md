# api.luxintimo.com

## Getting started

1. Install golang
2. Create go directory in your GOPATH (type go env to see where it is)
3. Create src, bin, pkg folders
4. git clone the repo inside src
5. `go mod init`
8. `go get ./...`
6. `go get github.com/SermoDigital/jose@master` (only there are get errors)
7. `go get -u github.com/satori/go.uuid@master` (only there are get errors)
9. Install PostgreSQL
10. Set password to 123 for development user
11. Add Environment paths
 - C:\Program Files\PostgreSQL\14\bin
 - C:\Program Files\PostgreSQL\14\lib
12. run `create_schema.sh` script
13. run `go build && ./api.luxurycars.exe admin dir` to run the server and add admin user & directory folder (for images)
14. enjoy