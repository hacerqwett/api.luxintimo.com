package config

import (
	"crypto/rsa"
	"encoding/json"
	"io/ioutil"

	"api.luxintimo.com/log"
	"github.com/SermoDigital/jose/crypto"
)

var (
	// Config Global config object
	Config = new(configuration)
	// ConfigPath Path to the config.json file
	ConfigPath = "./config.json"
)

type configuration struct {
	HTTPHost            string `json:"http_host"`
	DbHost              string `json:"db_host"`
	DbName              string `json:"db_name"`
	DbUsername          string `json:"db_username"`
	DbPassword          string `json:"db_password"`
	AdminUsername       string `json:"admin_username"`
	AdminEmail          string `json:"admin_email"`
	AdminPassword       string `json:"admin_password"`
	ActivationURL       string `json:"activation_url"`
	NoReplySender       string `json:"no_reply_sender"`
	SMTPHost            string `json:"smtp_host"`
	SMTPPort            int    `json:"smtp_port"`
	SMTPUsername        string `json:"smtp_username"`
	SMTPPassword        string `json:"smtp_password"`
	RSAPrivateKeyPath   string `json:"rsa_private_path"`
	RSAPublicKeyPath    string `json:"rsa_public_path"`
	RSAPrivateKey       *rsa.PrivateKey
	RSAPublicKey        *rsa.PublicKey
	JWTExpirationHours  int    `json:"jwt_expiration_hours"`
	StaticFilesPath     string `json:"static_files_path"`
	StaticFilesEndpoint string `json:"static_files_endpoint"`
	FormFileKey         string `json:"form_file_key"`
	MaxFileSize         int64  `json:"max_file_size"`
	CollectionSize      int64  `json:"collection_size"`
}

// Init - Initialize config
func Init() error {
	data, err := ioutil.ReadFile(ConfigPath)
	if err != nil {
		log.Error("Failed to Read File from ConfigPath", err)
		return err
	}

	if err = json.Unmarshal(data, Config); err != nil {
		log.Error("Failed to Unmarshal Config", err)
		return err
	}

	priv, err := ioutil.ReadFile(Config.RSAPrivateKeyPath)
	if err != nil {
		log.Error("Failed to Read File from RSA Private Key Path", err)
		return err
	}

	pub, err := ioutil.ReadFile(Config.RSAPublicKeyPath)
	if err != nil {
		log.Error("Failed to Read File from RSA Public Key Path", err)
		return err
	}

	Config.RSAPrivateKey, err = crypto.ParseRSAPrivateKeyFromPEM(priv)
	if err != nil {
		log.Error("Failed to Parse RSA Private Key", err)
		return err
	}

	Config.RSAPublicKey, err = crypto.ParseRSAPublicKeyFromPEM(pub)
	if err != nil {
		log.Error("Failed to Parse RSA Public Key", err)
		return err
	}

	return nil
}
