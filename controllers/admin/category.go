package admin

import (
	"net/http"

	"api.luxintimo.com/db"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/rest"
	"api.luxintimo.com/utils/uhttp"

	"api.luxintimo.com/models"
)

// CreateCategoryHandler - Registers Category in the DB; Returns Category as JSON.
func CreateCategoryHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.CategoryRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Category{
		Name:   request.Name,
		Gender: request.Gender,
	}

	id, err := models.CreateCategory(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateCategoryHandler - Updates Category in the DB; Returns Category as JSON.
func UpdateCategoryHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.CategoryRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if _, err := models.GetCategory(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Category{
		ID:     db.NewNullInt64(id),
		Name:   request.Name,
		Gender: request.Gender,
	}

	if err := models.UpdateCategory(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteCategoryHandler - Deletes Category from DB
func DeleteCategoryHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteCategory(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
