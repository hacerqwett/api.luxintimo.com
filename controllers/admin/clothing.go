package admin

import (
	"net/http"

	"api.luxintimo.com/utils/constants"

	"api.luxintimo.com/db"
	"api.luxintimo.com/rest"

	"api.luxintimo.com/models"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/uhttp"
)

// CreateClothingHandler - Registers Clothing in the DB; Returns Clothing as JSON.
func CreateClothingHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ClothingRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Clothing{
		Name:           request.Name,
		ManufacturerID: request.ManufacturerID,
		SubCategoryID:  request.SubCategoryID,
		Status:         db.NewNullInt64(constants.StatusDraft),
	}

	id, err := models.CreateClothing(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	cd := &models.ClothingDetails{
		ClothingID:  db.NewNullInt64(id),
		Description: request.Description,
	}

	err = models.CreateClothingDetails(cd)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateClothingHandler - Updates Clothing in the DB; Returns OK on success.
func UpdateClothingHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ClothingRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if request.Status.Int64 == constants.StatusLive {
		if err := request.Validate(); err != nil {
			uhttp.WriteError(w, err)
			return
		}
	}

	c := &models.Clothing{
		ID:             db.NewNullInt64(id),
		Name:           request.Name,
		ManufacturerID: request.ManufacturerID,
		SubCategoryID:  request.SubCategoryID,
		Status:         request.Status,
		Price:          request.Price,
		Discount:       request.Discount,
	}

	if err := models.UpdateClothing(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	cd := &models.ClothingDetails{
		ClothingID:  db.NewNullInt64(id),
		Description: request.Description,
	}

	err = models.UpdateClothingDetails(cd)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteClothingHandler - Deletes Clothing from DB
func DeleteClothingHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteClothing(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
