package admin

import (
	"net/http"

	"api.luxintimo.com/rest"

	"api.luxintimo.com/models"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/uhttp"
)

// CreateClothingDetailsHandler - Registers Clothing Details in the DB; Returns Clothing Details as JSON.
func CreateClothingDetailsHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ClothingDetailsRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.ClothingDetails{
		ClothingID:  request.ClothingID,
		Description: request.Description,
	}

	err = models.CreateClothingDetails(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// UpdateClothingDetailsHandler - Updates Clothing Details in the DB; Returns Clothing Details as JSON.
func UpdateClothingDetailsHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ClothingDetailsRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if _, err := models.GetClothingDetails(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.ClothingDetails{
		ClothingID:  request.ClothingID,
		Description: request.Description,
	}

	if err := models.UpdateClothingDetails(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteClothingDetailsHandler - Deletes Clothing Details from DB
func DeleteClothingDetailsHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteClothingDetails(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
