package admin

import (
	"net/http"

	"api.luxintimo.com/db"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/rest"
	"api.luxintimo.com/utils/uhttp"

	"api.luxintimo.com/models"
)

// CreateColorHandler - Registers Color in the DB; Returns Color ID
func CreateColorHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ColorRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Color{
		Name: request.Name,
		HEX:  request.HEX,
	}

	id, err := models.CreateColor(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateColorHandler - Updates Color in the DB; Returns Color as JSON.
func UpdateColorHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ColorRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if _, err := models.GetColor(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Color{
		ID:   db.NewNullInt64(id),
		Name: request.Name,
		HEX:  request.HEX,
	}

	if err := models.UpdateColor(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteColorHandler - Deletes Color from DB
func DeleteColorHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteColor(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
