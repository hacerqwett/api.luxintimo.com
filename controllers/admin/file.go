package admin

import (
	"net/http"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/rest"

	"api.luxintimo.com/models"
	"api.luxintimo.com/utils/file"
	"api.luxintimo.com/utils/uhttp"
)

// UploadFileHandler - Uploads File in Server, returns ID & Name
func UploadFileHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := file.ValidateSize(w, r); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	f, err := file.CreateFromRequest(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	id, err := models.CreateFile(f)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	res := rest.UploadFileResponse{ID: id, Name: f.Name.String}

	uhttp.WriteJSON(w, res)
}

// DeleteFileHandler - Deletes File in DB & OS, returns OK
func DeleteFileHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	f, err := models.GetFile(id)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteFile(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	path := file.BuildPath(f.Name.String)

	if err := file.Delete(path); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
