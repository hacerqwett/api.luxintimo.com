package admin

import (
	"encoding/json"
	"net/http"

	"api.luxintimo.com/rerr"

	"api.luxintimo.com/db"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/file"

	"api.luxintimo.com/rest"

	"api.luxintimo.com/models"
	"api.luxintimo.com/utils/uhttp"
)

// UploadImageHandler - Uploads Clothing Image in Server, returns ID & Name
func UploadImageHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := file.ValidateSize(w, r); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	data, err := file.GetRequestData(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.UploadImageRequest)
	if err := json.Unmarshal([]byte(data), request); err != nil {
		uhttp.WriteError(w, rerr.UnknownRequestStructure)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	f, err := file.CreateFromRequest(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	fid, err := models.CreateFile(f)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := file.CompressImage(f.Name.String, request.Aspect.Int64); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Image{
		ClothingID: request.ClothingID,
		FileID:     db.NewNullInt64(fid),
		Aspect:     request.Aspect,
	}

	cid, err := models.CreateImage(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	res := rest.UploadImageResponse{ID: cid, Name: f.Name.String, Aspect: request.Aspect.Int64}

	uhttp.WriteJSON(w, res)
}

// UpdateImageHandler - Updates Clothing Image in Server, returns OK
func UpdateImageHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.UpdateImageRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	u := &models.Image{
		ID:      db.NewNullInt64(id),
		ColorID: request.ColorID,
	}

	if err := models.UpdateImage(u); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteImageHandler - Deletes Clothing Image in DB & OS, returns OK
func DeleteImageHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c, err := models.GetImage(id)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	f, err := models.GetFile(c.FileID.Int64)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteImage(c.ID.Int64); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteFile(f.ID.Int64); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	path := file.BuildPath("*" + f.Name.String)

	if err := file.Delete(path); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
