package admin

import (
	"net/http"

	"api.luxintimo.com/rest"

	"api.luxintimo.com/db"

	"api.luxintimo.com/models"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/uhttp"
)

// CreateManufacturerHandler - Registers Manufacturer in the DB; Returns Manufacturer as JSON.
func CreateManufacturerHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ManufacturerRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Manufacturer{
		Name: request.Name,
	}

	id, err := models.CreateManufacturer(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateManufacturerHandler - Updates Manufacturer in the DB; Returns Manufacturer as JSON.
func UpdateManufacturerHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.ManufacturerRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if _, err := models.GetManufacturer(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Manufacturer{
		ID:   db.NewNullInt64(id),
		Name: request.Name,
	}

	if err := models.UpdateManufacturer(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteManufacturerHandler - Deletes Manufacturer from DB
func DeleteManufacturerHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteManufacturer(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
