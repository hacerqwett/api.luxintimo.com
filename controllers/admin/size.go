package admin

import (
	"net/http"

	"api.luxintimo.com/db"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/rest"
	"api.luxintimo.com/utils/uhttp"

	"api.luxintimo.com/models"
)

// CreateSizeHandler - Registers Size in the DB; Returns Size ID
func CreateSizeHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.SizeRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Size{
		ClothingID: request.ClothingID,
		ColorID:    request.ColorID,
		Size:       request.Size,
		Count:      request.Count,
	}

	id, err := models.CreateSize(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateSizeHandler - Updates Size in the DB; Returns Size as JSON.
func UpdateSizeHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.SizeRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.Size{
		ID:         db.NewNullInt64(id),
		ClothingID: request.ClothingID,
		ColorID:    request.ColorID,
		Size:       request.Size,
		Count:      request.Count,
	}

	if err := models.UpdateSize(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}

// DeleteSizeHandler - Deletes Size from DB
func DeleteSizeHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteSize(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
