package admin

import (
	"net/http"

	"api.luxintimo.com/rest"

	"api.luxintimo.com/db"

	"api.luxintimo.com/models"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/uhttp"
)

// CreateSubCategoryHandler - Registers SubCategory in the DB; Returns SubCategory as JSON.
func CreateSubCategoryHandler(w http.ResponseWriter, r *http.Request) {
	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.SubCategoryRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.SubCategory{
		Name:       request.Name,
		CategoryID: request.CategoryID,
	}

	id, err := models.CreateSubCategory(c)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// UpdateSubCategoryHandler - Updates SubCategory in the DB; Returns SubCategory as JSON.
func UpdateSubCategoryHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	request := new(rest.SubCategoryRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if _, err := models.GetSubCategory(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	c := &models.SubCategory{
		ID:         db.NewNullInt64(id),
		Name:       request.Name,
		CategoryID: request.CategoryID,
	}

	if err := models.UpdateSubCategory(c); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, id)
}

// DeleteSubCategoryHandler - Deletes SubCategory from DB
func DeleteSubCategoryHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserFromHeader(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateAdmin(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := models.DeleteSubCategory(id); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteOK(w)
}
