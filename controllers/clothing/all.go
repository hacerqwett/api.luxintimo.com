package clothing

import (
	"net/http"

	"api.luxintimo.com/rest"
	"api.luxintimo.com/utils/uhttp"

	"api.luxintimo.com/models"
)

// GetAllHandler - Returns Categories, SubCategories, Manufacturers, Clothes as JSON.
func GetAllHandler(w http.ResponseWriter, r *http.Request) {
	categories, err := models.GetAllCategories()
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	subcategories, err := models.GetSubCategories()
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	manufacturers, err := models.GetManufacturers()
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	colors, err := models.GetAllColors()
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	res := rest.AllResponse{
		Categories:    categories,
		SubCategories: subcategories,
		Manufacturers: manufacturers,
		Colors:        colors,
	}

	uhttp.WriteJSON(w, res)
}
