package clothing

import (
	"net/http"

	"api.luxintimo.com/config"
	"api.luxintimo.com/models"
	"api.luxintimo.com/rest"

	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/constants"
	"api.luxintimo.com/utils/uhttp"
)

// GetClothingHandler - Returns Clothings as JSON.
func GetClothingHandler(w http.ResponseWriter, r *http.Request) {
	id, err := auth.GetValidID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	clothing, err := models.GetFullClothing(id)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	// if clothing.Clothing.Status.Int64 == constants.StatusDraft {
	// 	uhttp.WriteError(w, rerr.NotFound)
	// 	return
	// }

	uhttp.WriteJSON(w, clothing)
}

// GetFilteredClothingHandler - Returns Filtered Clothings as JSON.
func GetFilteredClothingHandler(w http.ResponseWriter, r *http.Request) {
	index, err := auth.GetValidIndex(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	query := r.URL.Query()

	params := &rest.FilterParamsRequest{
		Category:      query.Get("category"),
		Gender:        query.Get("gender"),
		SubCategories: query["subcat"],
		Sizes:         query["sizes"],
		Colors:        query["colors"],
		Manufacturers: query["brand"],
		Sort:          query.Get("sort"),
		MinPrice:      query.Get("minPrice"),
		MaxPrice:      query.Get("maxPrice"),
	}

	err = params.Validate()
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	p := &models.FilterParams{
		Status:        int64(constants.StatusDraft),
		Offset:        index * config.Config.CollectionSize,
		Limit:         config.Config.CollectionSize,
		Category:      params.Category,
		Gender:        params.Gender,
		SubCategories: params.SubCategories,
		Sizes:         params.Sizes,
		Colors:        params.Colors,
		Manufacturers: params.Manufacturers,
		Sort:          params.Sort,
		MinPrice:      params.MinPrice,
		MaxPrice:      params.MaxPrice,
	}

	clothing, err := models.GetFilteredClothing(p)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	previews, err := models.GetClothingPreviewFromClothing(clothing)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	uhttp.WriteJSON(w, previews)
}
