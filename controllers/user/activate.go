package user

import (
	"net/http"

	"api.luxintimo.com/log"
	"api.luxintimo.com/models"
	"api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/uhttp"
)

// ActivateUserHandler - Activates User from URL key
func ActivateUserHandler(w http.ResponseWriter, r *http.Request) {
	key, err := auth.GetValidUUID(r)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	email, err := models.ActivateUser(key)
	if err != nil {
		uhttp.WriteError(w, rerr.EmailRequired)
		return
	}

	log.Notice("Successfully activated user: ", email)

	uhttp.WriteOK(w)
}
