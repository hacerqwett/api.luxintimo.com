package user

import (
	"net/http"

	"api.luxintimo.com/db"
	"api.luxintimo.com/models"
	"api.luxintimo.com/rest"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/utils/uhttp"
)

// LoginUserHandler - Returns user with token
func LoginUserHandler(w http.ResponseWriter, r *http.Request) {
	request := new(rest.LoginUserRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user, err := models.GetUserByEmail(request.Email.String)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := user.ValidateActivated(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := auth.BCryptValidatePassword(user.Password.String, request.Password.String); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	j := auth.JWTCreate(user.ID.Int64)

	js, err := auth.JWTString(j)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	t, err := models.UpdateUserLastLogin(user.ID.Int64)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	user.LastLogin = db.NewNullString(t)

	res := rest.LoginUserResponse{User: user, Token: js}

	uhttp.WriteJSON(w, res)
}
