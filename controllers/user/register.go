package user

import (
	"net/http"

	"api.luxintimo.com/log"
	"api.luxintimo.com/models"
	"api.luxintimo.com/rerr"
	"api.luxintimo.com/rest"
	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/email"

	"api.luxintimo.com/db"
	"api.luxintimo.com/utils/uhttp"
)

// RegisterUserHandler - Registers User in the DB; Returns User as JSON.
func RegisterUserHandler(w http.ResponseWriter, r *http.Request) {
	request := new(rest.RegisterUserRequest)
	if err := uhttp.ReadJSON(r, request); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if err := request.Validate(); err != nil {
		uhttp.WriteError(w, err)
		return
	}

	if user, err := models.GetUserByEmail(request.Email.String); user != nil {
		uhttp.WriteError(w, rerr.UserAlreadyExists)
		return
	} else if err != nil && err != rerr.InvalidEmailOrPassword {
		uhttp.WriteError(w, rerr.InternalServerError)
		return
	}

	hash, err := auth.BCryptPassword(request.Password1.String)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	reqUser := &models.User{
		Email:    request.Email,
		Password: db.NewNullString(hash),
		Address:  request.Address,
		Name:     request.Name,
		Phone:    request.Phone,
	}

	id, err := models.RegisterUser(reqUser)
	if err != nil {
		uhttp.WriteError(w, err)
	}

	log.Notice("Successfully registered user: ", request.Email.String)

	key, err := models.NewActivation(id)
	if err != nil {
		uhttp.WriteError(w, err)
		return
	}

	go email.SendActivationEmail(request.Email.String, key)

	uhttp.WriteJSON(w, id)
}
