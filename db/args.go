package db

import (
	"errors"
	"os"

	"api.luxintimo.com/config"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/auth"
)

const scriptPath = "/scripts/shell/"

// RunArgs - Runs args from os.Args
func RunArgs() error {
	for _, arg := range os.Args {
		var err error

		switch arg {
		case "admin":
			err = RegisterAdmins()
		case "dir":
			err = CreateDirs()
		}

		if err != nil {
			return err
		}
	}

	return nil
}

// RegisterAdmins - Registers Admins from config
func RegisterAdmins() error {
	log.Notice("Registering admins...")

	password, authErr := auth.BCryptPassword(config.Config.AdminPassword)
	if authErr != nil {
		log.Error("Failed to register admin")
		return errors.New("Failed to register admin")
	}

	_, exErr := DB.Exec(`INSERT INTO users(email, name, password, address, activated, admin)
							VALUES ($1, $2, $3, $4, $5, $6)`,
		config.Config.AdminEmail, config.Config.AdminUsername, password, "", true, true)

	if rerr.IsAlreadyExists(exErr) {
		log.Notice("Admin already exists. Skipping")
		return nil
	}

	if exErr != nil {
		log.Error("Failed to register admin: ", exErr)
		return exErr
	}

	log.Notice("Successfully registered admins")

	return nil
}

// CreateDirs - Creates Needed directories
func CreateDirs() error {
	log.Notice("Creating directories...")

	path := config.Config.StaticFilesPath

	if _, err := os.Stat(path); err == nil {
		log.Notice("Directories already exist. Skipping")
		return nil
	} else if !os.IsNotExist(err) {
		log.Error("Failed to check if dirs exist: ", err)
		return err
	}

	if err := os.Mkdir(path, 0766); err != nil {
		log.Error("Failed to create dirs: ", err)
		return err
	}

	log.Notice("Successfully created directories")

	return nil
}
