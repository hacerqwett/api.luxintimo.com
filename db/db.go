package db

import (
	"api.luxintimo.com/config"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // Postgresql driver
)

var (
	// DB Global DB instance
	DB *sqlx.DB
)

// Init Initialize database connection
func Init() error {
	var err error
	var datasource = "postgres://" + config.Config.DbUsername + ":" + config.Config.DbPassword + "@" + config.Config.DbHost + "/" + config.Config.DbName + "?sslmode=disable"

	DB, err = sqlx.Connect("postgres", datasource)
	if err != nil {
		log.Error("Failed to connect to database: ", err)
		return err
	}

	if err = DB.Ping(); err != nil {
		log.Error("Failed to ping database: ", err)
		return err
	}

	return nil
}

// RunInTx Run code in transaction
func RunInTx(handler func(tx *sqlx.Tx) *rerr.RestError) *rerr.RestError {
	var err *rerr.RestError

	tx, bErr := DB.Beginx()
	if bErr != nil {
		log.Error("Failed to Begin Transaction")
		err = rerr.InternalServerError
		return err
	}

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		} else if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	err = handler(tx)
	return err
}
