package db

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"time"
)

var nullString = []byte("null")

// NullString Null String Struct
type NullString struct {
	sql.NullString
}

// NewNullString Create New Null String
func NewNullString(v interface{}) (n NullString) {
	n.Scan(v)
	return
}

// IsValid - Invalid if empty or null
func (s *NullString) IsValid() bool {
	return s.Valid && len(s.String) > 0
}

// MergeString - merges two NullString, returns n1 if n1.IsValid(), n2 if not
func MergeString(n1 NullString, n2 NullString) NullString {
	if n1.IsValid() {
		return n1
	}

	return n2
}

// MarshalJSON Marshal Null String
func (s NullString) MarshalJSON() ([]byte, error) {
	if s.IsValid() {
		return json.Marshal(s.String)
	}
	return nullString, nil
}

// UnmarshalJSON Unmarshal Null String
func (s *NullString) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	return s.Scan(v)
}

// NullInt64 Null Int64 Struct
type NullInt64 struct {
	sql.NullInt64
}

// NewNullInt64 Create New Null Int64
func NewNullInt64(v interface{}) (n NullInt64) {
	n.Scan(v)
	return
}

// IsValid - Invalid if less than 0 or null
func (n *NullInt64) IsValid() bool {
	return n.Valid && n.Int64 > 0
}

// AreValid - Validates ID
func AreValid(arr []NullInt64) bool {
	if len(arr) < 1 {
		return false
	}

	for _, n := range arr {
		if !n.IsValid() {
			return false
		}
	}

	return true
}

// MergeInt64 - merges two NullInt64, returns n1 if n1.IsValid(), n2 if not
func MergeInt64(n1 NullInt64, n2 NullInt64) NullInt64 {
	if n1.IsValid() {
		return n1
	}

	return n2
}

// MarshalJSON Marshal Null Int64
func (n NullInt64) MarshalJSON() ([]byte, error) {
	if n.Valid {
		return json.Marshal(n.Int64)
	}
	return nullString, nil
}

// UnmarshalJSON Unmarshal Null Int64
func (n *NullInt64) UnmarshalJSON(b []byte) error {
	var s json.Number
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	if s.String() == "" {
		return nil
	}
	return n.Scan(s)
}

// NullTime Null Time Struct
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// NewNullTime Create New Null Time
func NewNullTime(v interface{}) (n NullTime) {
	n.Scan(v)
	return
}

// Scan Implements the Scanner interface.
func (t *NullTime) Scan(value interface{}) error {
	if value == nil {
		t.Valid = false
		return nil
	}
	switch tp := value.(type) {
	case time.Time:
		t.Valid = true
		t.Time = tp
	default:
		t.Valid = false
	}
	return nil
}

// Value Implements the driver Valuer interface.
func (t NullTime) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}

// NullBool Null Bool Struct
type NullBool struct {
	sql.NullBool
}

// IsValidAndTrue - IsValidAndTrue
func (n *NullBool) IsValidAndTrue() bool {
	return n.Valid && n.Bool
}

// NewNullBool Create New Null Int64
func NewNullBool(v interface{}) (n NullBool) {
	n.Scan(v)
	return
}

// MergeBool - merges two NullBool, returns n1 if n1.Valid, n2 if not
func MergeBool(n1 NullBool, n2 NullBool) NullBool {
	if n1.Valid {
		return n1
	}

	return n2
}

// MarshalJSON Marshal NullBool
func (n NullBool) MarshalJSON() ([]byte, error) {
	if n.Valid {
		return json.Marshal(n.Bool)
	}
	return nullString, nil
}

// UnmarshalJSON Unmarshal Null Int64
func (n *NullBool) UnmarshalJSON(b []byte) error {
	var s interface{}
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	return n.Scan(s)
}

// NullFloat64 Null Float64 Struct
type NullFloat64 struct {
	sql.NullFloat64
}

// NewNullFloat64 Create New Null Float64
func NewNullFloat64(v interface{}) (n NullFloat64) {
	n.Scan(v)
	return
}

// MarshalJSON Marshal Null Float64
func (n NullFloat64) MarshalJSON() ([]byte, error) {
	if n.Valid {
		return json.Marshal(n.Float64)
	}
	return nullString, nil
}

// UnmarshalJSON Unmarshal Null Float64
func (n *NullFloat64) UnmarshalJSON(b []byte) error {
	var s interface{}
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	return n.Scan(s)
}

// IsValid - Invalid if positive or null
func (n *NullFloat64) IsValid() bool {
	return n.Valid && n.Float64 > 0
}

// IsNotNegative - Invalid if empty or not negative
func (n *NullFloat64) IsNotNegative() bool {
	return n.Valid && n.Float64 > -1
}
