package log

import (
	"os"

	logging "github.com/op/go-logging"
)

// Logger Global Logger
var logger = logging.MustGetLogger("lux")

// Init - Init logger
func Init() {
	backend := logging.NewLogBackend(os.Stdout, "", 1)

	format := logging.MustStringFormatter(
		`%{color}%{time:2006-01-02 15:04:05} %{shortfile} %{longfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)

	backendFormatter := logging.NewBackendFormatter(backend, format)
	logging.SetBackend(backendFormatter)
	logger.ExtraCalldepth = 1
}

// Error - Log error
func Error(args ...interface{}) {
	logger.Error(args...)
}

// Warning - Log warning
func Warning(args ...interface{}) {
	logger.Warning(args...)
}

// Info - Log info
func Info(args ...interface{}) {
	logger.Info(args...)
}

// Notice - Log Notice
func Notice(args ...interface{}) {
	logger.Notice(args...)
}

// Debug - Log Notice
func Debug(args ...interface{}) {
	logger.Debug(args...)
}
