package main

import (
	"os"

	"api.luxintimo.com/config"
	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	"api.luxintimo.com/server"
)

func main() {
	log.Init()

	if err := config.Init(); err != nil {
		os.Exit(1)
	}

	if err := db.Init(); err != nil {
		os.Exit(1)
	}

	if err := db.RunArgs(); err != nil {
		os.Exit(1)
	}

	if err := server.Start(); err != nil {
		os.Exit(1)
	}
}
