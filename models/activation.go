package models

import (
	"database/sql"

	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/utime"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	"github.com/jmoiron/sqlx"
)

// Activation - Activation Model
type Activation struct {
	ID            db.NullInt64  `db:"id, pkey" json:"id"`
	DateSent      db.NullTime   `db:"date_sent" json:"date_sent"`
	DateActivated db.NullTime   `db:"date_activated" json:"date_activated"`
	Key           db.NullString `db:"key" json:"key"`
	UserID        db.NullInt64  `db:"user_id" json:"user_id"`
}

// NewActivation - Inserts new activation into db, returns activation key
func NewActivation(userID int64) (string, *rerr.RestError) {
	key, keyErr := auth.GenerateUUID()
	if keyErr != nil {
		return "", keyErr
	}

	_, err := db.DB.Exec(`INSERT INTO activation(key, user_id) VALUES($1, $2)`, key, userID)

	if rerr.IsAlreadyExists(err) {
		log.Error("Failed to create activation. Already exists for user: ", userID, err)
		return "", rerr.FailedToCreateActivation
	}

	if err != nil {
		log.Error("Failed to create activation for user: ", userID, err)
		return "", rerr.FailedToCreateActivation
	}

	return key, nil
}

// ActivateUser - Activates User, updates activation, returns email
func ActivateUser(key string) (string, *rerr.RestError) {
	var email string

	err := db.RunInTx(func(tx *sqlx.Tx) *rerr.RestError {

		id, err := UpdateActivationTx(tx, key)
		if err != nil {
			return err
		}

		email, err = UserSetActivatedTx(tx, id)
		if err != nil {
			return err
		}

		return nil
	})

	return email, err
}

// UpdateActivationTx - Updates activation in transaction, returns user email
func UpdateActivationTx(tx *sqlx.Tx, key string) (int64, *rerr.RestError) {
	var userID int64
	err := tx.QueryRow(`UPDATE activation SET date_activated=$1 WHERE key=$2 RETURNING user_id`, utime.GetTime(), key).Scan(&userID)

	if err == sql.ErrNoRows {
		log.Warning("Failed to activate user. No rows returned from key: ", key)
		return -1, rerr.InvalidActivationKey
	}

	if err != nil {
		log.Error("Failed to activate user from key: ", key, err)
		return -1, rerr.FailedToActivateUser
	}

	return userID, nil
}

// UserSetActivatedTx - Sets user activated to true, returns User email
func UserSetActivatedTx(tx *sqlx.Tx, id int64) (string, *rerr.RestError) {
	var email string

	err := db.DB.QueryRow(`UPDATE users SET activated=true WHERE id=$1 
							RETURNING email`, id).Scan(&email)

	if err == sql.ErrNoRows {
		log.Warning("Failed to activate user. No rows returned from id: ", id)
		return "", rerr.FailedToActivateUser
	}

	if err != nil {
		log.Error("Error activating user: ", err)
		return "", rerr.FailedToActivateUser
	}

	return email, nil
}
