package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Category - Category Struct
type Category struct {
	ID     db.NullInt64  `db:"id, pkey" json:"id"`
	Name   db.NullString `db:"name" json:"name"`
	Gender db.NullInt64  `db:"gender" json:"gender"`
}

// CreateCategory - Creates Category
func CreateCategory(c *Category) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO category(name, gender) VALUES ($1, $2) RETURNING id`, c.Name, c.Gender).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create Category. Already exists %#v", c))
		return -1, rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Category %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetCategory - Gets Category from id
func GetCategory(id int64) (*Category, *rerr.RestError) {
	category := new(Category)

	err := db.DB.Get(category, `SELECT id, name, gender FROM category WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Category. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Categories from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return category, nil
}

// GetAllCategories - Gets all Categories
func GetAllCategories() ([]Category, *rerr.RestError) {
	categories := make([]Category, 0)
	err := db.DB.Select(&categories, `SELECT id, name, gender FROM category`)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Categories. No rows returned.")
		return categories, nil
	}

	if err != nil {
		log.Error("Failed to get Categories: ", err)
		return nil, rerr.InternalServerError
	}

	return categories, nil
}

// UpdateCategory - Updates Category
func UpdateCategory(c *Category) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE category SET name=$2, gender=$3 WHERE id=$1`, c.ID, c.Name, c.Gender)

	if err != nil {
		log.Error("Failed to update Category from id: ", c.ID.Int64, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to update Category from id: ", c.ID.Int64, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Category. No rows returned from id: ", c.ID.Int64)
		return rerr.CategoryNotFound
	}

	return nil
}

// DeleteCategory - Deletes Category
func DeleteCategory(categoryID int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM category WHERE id=$1`, categoryID)

	if err != nil {
		log.Error("Failed to get Categories from id: ", categoryID, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to get Categories from id: ", categoryID, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Error("Failed to get Categories. No rows returned from id: ", categoryID)
		return rerr.CategoryNotFound
	}

	return nil
}
