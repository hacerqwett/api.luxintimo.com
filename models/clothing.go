package models

import (
	"database/sql"
	"fmt"
	"strings"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Clothing - Clothing Struct
type Clothing struct {
	ID             db.NullInt64   `db:"id, pkey" json:"id"`
	Name           db.NullString  `db:"name" json:"name"`
	ManufacturerID db.NullInt64   `db:"manufacturer_id" json:"manufacturer_id"`
	SubCategoryID  db.NullInt64   `db:"subcategory_id" json:"subcategory_id"`
	Status         db.NullInt64   `db:"status" json:"status"`
	Price          db.NullFloat64 `db:"price" json:"price"`
	Discount       db.NullFloat64 `db:"discount" json:"discount"`
}

// ClothingPreview - ClothingPreview Struct
type ClothingPreview struct {
	*Clothing
	Images []*ImageResponse `db:"image" json:"images"`
	Sizes  []*Size          `db:"size" json:"sizes"`
}

// FullClothing - FullClothing Struct
type FullClothing struct {
	Clothing *ClothingPreview `json:"clothing"`
	Details  *ClothingDetails `json:"details"`
}

// FilterParams - FilterParams Struct
type FilterParams struct {
	Status        int64    `json:"status"`
	Limit         int64    `json:"limit"`
	Offset        int64    `json:"offset"`
	Category      string   `json:"category"`
	Gender        string   `json:"gender"`
	SubCategories []string `json:"subCategories"`
	Sizes         []string `json:"sizes"`
	Colors        []string `json:"colors"`
	Manufacturers []string `json:"manufacturers"`
	Sort          string   `json:"sort"`
	MinPrice      string   `json:"minPrice"`
	MaxPrice      string   `json:"maxPrice"`
}

// CreateClothing - Creates Clothing
func CreateClothing(c *Clothing) (int64, *rerr.RestError) {
	var id int64

	err := db.DB.QueryRow(`INSERT INTO clothing(name, manufacturer_id, subcategory_id, status, price, discount) 
							VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, c.Name, c.ManufacturerID, c.SubCategoryID, c.Status, c.Price, c.Discount).Scan(&id)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Clothing %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetClothing - Gets Clothing
func GetClothing(id int64) (*Clothing, *rerr.RestError) {
	clothing := new(Clothing)

	err := db.DB.Get(clothing, `SELECT id, name, manufacturer_id, subcategory_id, status, price, discount FROM clothing WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Clothing. No rows returned from id: ", id)
		return nil, rerr.ClothingNotFound
	}

	if err != nil {
		log.Error("Failed to get Clothing from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return clothing, nil
}

// GetClothingByStatus - Gets All Clothing by status
func GetClothingByStatus(status int, limit int64, offset int64) ([]*Clothing, *rerr.RestError) {
	clothing := make([]*Clothing, 0)

	err := db.DB.Select(&clothing, `SELECT id, name, manufacturer_id, subcategory_id, status, price, discount 
												FROM clothing 
												WHERE status=$1
												LIMIT $2 OFFSET $3`, status, limit, offset)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get All Clothing. No rows returned")
		return clothing, nil
	}

	if err != nil {
		log.Error("Failed to get All Clothing", err)
		return nil, rerr.InternalServerError
	}

	return clothing, nil
}

// UpdateClothing - Updates Clothing
func UpdateClothing(c *Clothing) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE clothing SET name=$2, manufacturer_id=$3, subcategory_id=$4, status=$5, price=$6, discount=$7
							WHERE id=$1`, c.ID, c.Name, c.ManufacturerID, c.SubCategoryID, c.Status, c.Price, c.Discount)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Clothing. No rows returned from id: ", c.ID.Int64)
		return rerr.ClothingNotFound
	}

	return nil
}

// DeleteClothing - Deletes Clothing
func DeleteClothing(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM clothing WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Clothing from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Clothing from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Clothing. No rows returned from id: ", id)
		return rerr.ClothingNotFound
	}

	return nil
}

// GetClothingPreview - Gets ClothingPreview
func GetClothingPreview(id int64) (*ClothingPreview, *rerr.RestError) {
	p := new(ClothingPreview)
	var err *rerr.RestError

	if p.Clothing, err = GetClothing(id); err != nil {
		return nil, err
	}

	if p.Images, err = GetAllImages(id); err != nil {
		return nil, err
	}

	if p.Sizes, err = GetSizes(id); err != nil {
		return nil, err
	}

	return p, nil
}

// GetClothingPreviewFromClothing - GetClothingPreviewFromClothing
func GetClothingPreviewFromClothing(clothing []*Clothing) ([]*ClothingPreview, *rerr.RestError) {
	var err *rerr.RestError
	previews := make([]*ClothingPreview, 0)
	for _, c := range clothing {
		p := new(ClothingPreview)
		p.Clothing = c
		if p.Images, err = GetAllImages(c.ID.Int64); err != nil {
			return nil, err
		}
		if p.Sizes, err = GetSizes(c.ID.Int64); err != nil {
			return nil, err
		}
		previews = append(previews, p)
	}

	return previews, nil
}

// GetFullClothing - Gets FullClothing
func GetFullClothing(id int64) (*FullClothing, *rerr.RestError) {
	fc := new(FullClothing)
	var err *rerr.RestError

	if fc.Clothing, err = GetClothingPreview(id); err != nil {
		return nil, err
	}

	if fc.Details, err = GetClothingDetails(id); err != nil {
		return nil, err
	}

	return fc, nil
}

// GetFilteredClothing - Get Filtered Clothing
func GetFilteredClothing(params *FilterParams) ([]*Clothing, *rerr.RestError) {

	category := ""
	if params.Category != "" {
		category = fmt.Sprintf("AND ca.id = %s ", params.Category)
	}

	gender := ""
	if params.Gender != "" {
		gender = fmt.Sprintf("AND ca.gender = %s ", params.Gender)
	}

	subcategory := ""
	if len(params.SubCategories) > 0 {
		subcategory = fmt.Sprintf("AND c.subcategory_id in (%s) ", strings.Join(params.SubCategories, ","))
	}

	size := ""
	if len(params.Sizes) > 0 {
		size = fmt.Sprintf(`AND si."size" in (%s) `, strings.Join(params.Sizes, ","))
	}

	color := ""
	if len(params.Colors) > 0 {
		color = fmt.Sprintf("AND i.color_id in (%s) ", strings.Join(params.Colors, ","))
	}

	manufacturers := ""
	if len(params.Manufacturers) > 0 {
		manufacturers = fmt.Sprintf("AND m.id in (%s) ", strings.Join(params.Manufacturers, ","))
	}

	price := ""
	if params.MinPrice != "" && params.MaxPrice != "" {
		price = fmt.Sprintf("AND ((c.price between %s AND %s) OR (c.discount between %s AND %s))", params.MinPrice, params.MaxPrice, params.MinPrice, params.MaxPrice)
	}

	clothing := make([]*Clothing, 0)

	whereQuery := category + subcategory + gender + size + color + manufacturers + price

	err := db.DB.Select(&clothing, `
									SELECT c.id, c.name, c.manufacturer_id, c.subcategory_id, c.status, c.price, c.discount
										FROM clothing AS c
											LEFT JOIN image AS i ON i.clothing_id=c.id
											LEFT JOIN file AS f ON f.id = i.file_id
											LEFT JOIN subcategory AS s ON s.id=c.subcategory_id
											LEFT JOIN category AS ca ON ca.id=s.category_id
											LEFT JOIN size AS si ON si.clothing_id=c.id
											LEFT JOIN manufacturer AS m ON m.id=c.manufacturer_id
												WHERE status=$1
												`+whereQuery+`
												GROUP BY c.id
												ORDER BY c.id
												LIMIT $2 OFFSET $3`, params.Status, params.Limit, params.Offset)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Filtered Clothing. No rows returned")
		return clothing, nil
	}

	if err != nil {
		log.Error("Failed to get Filtered Clothing", err)
		return nil, rerr.InternalServerError
	}

	return clothing, nil
}
