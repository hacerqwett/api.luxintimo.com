package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// ClothingDetails - Clothing Details Struct
type ClothingDetails struct {
	ClothingID  db.NullInt64  `db:"clothing_id" json:"clothing_id"`
	Description db.NullString `db:"description" json:"description"`
}

// CreateClothingDetails - Creates Clothing Details
func CreateClothingDetails(c *ClothingDetails) *rerr.RestError {
	res, err := db.DB.Exec(`INSERT INTO clothing_details(clothing_id, description) VALUES ($1, $2)`, c.ClothingID, c.Description)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing Details %#v", c), err)
		return rerr.InternalServerError
	}

	_, err = res.RowsAffected()

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create Clothing Details. Already exists %#v", c))
		return rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Clothing Details %#v", c), err)
		return rerr.InternalServerError
	}

	return nil
}

// GetClothingDetails - Gets Clothing Details from id
func GetClothingDetails(id int64) (*ClothingDetails, *rerr.RestError) {
	size := new(ClothingDetails)

	err := db.DB.Get(size, `SELECT clothing_id, description FROM clothing_details WHERE clothing_id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Clothing Details. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Clothing Details from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return size, nil
}

// GetAllClothingDetails - Gets all Clothing Details
func GetAllClothingDetails() ([]ClothingDetails, *rerr.RestError) {
	sizes := make([]ClothingDetails, 0)
	err := db.DB.Select(&sizes, `SELECT id, clothing_id, description FROM clothing_details`)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Clothing Details. No rows returned.")
		return sizes, nil
	}

	if err != nil {
		log.Error("Failed to get Clothing Details: ", err)
		return nil, rerr.InternalServerError
	}

	return sizes, nil
}

// UpdateClothingDetails - Updates Clothing Details
func UpdateClothingDetails(c *ClothingDetails) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE clothing_details SET description=$2 WHERE clothing_id=$1`, c.ClothingID, c.Description)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing Details %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing Details %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Clothing Details. No rows returned from id: ", c.ClothingID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteClothingDetails - Deletes Clothing Details from ID
func DeleteClothingDetails(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM clothing_details WHERE clothing_id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Clothing Details from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Clothing Details from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Clothing Details. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}
