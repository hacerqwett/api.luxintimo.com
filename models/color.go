package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Color - Color Struct
type Color struct {
	ID   db.NullInt64  `db:"id, pkey" json:"id"`
	Name db.NullString `db:"name" json:"name"`
	HEX  db.NullString `db:"hex" json:"hex"`
}

// CreateColor - Creates Color
func CreateColor(c *Color) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO color(name, hex) VALUES ($1, $2) RETURNING id`, c.Name, c.HEX).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create Color. Already exists. %#v", c))
		return -1, rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Color. %#v: ", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetColor - Gets Color from id
func GetColor(id int64) (*Color, *rerr.RestError) {
	size := new(Color)

	err := db.DB.Get(size, `SELECT id, name, hex FROM color WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Color. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Color from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return size, nil
}

// GetAllColors - Gets all Colors
func GetAllColors() ([]Color, *rerr.RestError) {
	sizes := make([]Color, 0)
	err := db.DB.Select(&sizes, `SELECT id, name, hex FROM color`)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Colors. No rows returned.")
		return sizes, nil
	}

	if err != nil {
		log.Error("Failed to get Colors: ", err)
		return nil, rerr.InternalServerError
	}

	return sizes, nil
}

// UpdateColor - Updates Color
func UpdateColor(c *Color) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE color SET name=$2, hex=$3 WHERE id=$1`, c.ID, c.Name, c.HEX)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Color %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Color %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Color. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteColor - Deletes Color from ID
func DeleteColor(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM color WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Color from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Color from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Color. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}
