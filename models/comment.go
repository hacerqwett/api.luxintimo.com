package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Comment - Comment Struct
type Comment struct {
	ID         db.NullInt64  `db:"id, pkey" json:"id"`
	ClothingID db.NullInt64  `db:"clothing_id" json:"clothing_id"`
	UserID     db.NullInt64  `db:"user_id" json:"user_id"`
	Comment    db.NullString `db:"comment" json:"comment"`
}

// CreateComment - Creates Comment
func CreateComment(c *Comment) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO comment(clothing_id, user_id, comment) VALUES ($1, $2, $3) RETURNING id`, c.ClothingID, c.UserID, c.Comment).Scan(&id)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Comment. %#v: ", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetComment - Gets Comment from id
func GetComment(id int64) (*Comment, *rerr.RestError) {
	size := new(Comment)

	err := db.DB.Get(size, `SELECT id, clothing_id, user_id, comment FROM comment WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Comment. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Comment from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return size, nil
}

// GetAllComments - Gets all Comments
func GetAllComments() ([]Comment, *rerr.RestError) {
	sizes := make([]Comment, 0)
	err := db.DB.Select(&sizes, `SELECT id, clothing_id, user_id, comment FROM comment`)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Comment. No rows returned.")
		return sizes, nil
	}

	if err != nil {
		log.Error("Failed to get Comment: ", err)
		return nil, rerr.InternalServerError
	}

	return sizes, nil
}

// UpdateComment - Updates Comment
func UpdateComment(c *Comment) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE comment SET clothing_id=$2, user_id=$3, comment=$4 WHERE id=$1`, c.ID, c.ClothingID, c.UserID, c.Comment)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Comment %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Comment %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Comment. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteComment - Deletes Comment from ID
func DeleteComment(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM comment WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Comment from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Comment from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Comment. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}
