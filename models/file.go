package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
	"github.com/jmoiron/sqlx"
)

// File - File struct
type File struct {
	ID           db.NullInt64  `db:"id"`
	Name         db.NullString `db:"name"`
	MIME         db.NullInt64  `db:"mime"`
	Size         db.NullInt64  `db:"size"`
	OriginalName db.NullString `db:"original_name"`
	DateCreated  db.NullTime   `db:"date_created"`
}

// CreateFile - Creates file in DB, returns id
func CreateFile(f *File) (int64, *rerr.RestError) {
	var id int64

	err := db.DB.QueryRow(`INSERT INTO file(name, mime, size, original_name) 
							VALUES ($1, $2, $3, $4) RETURNING id`,
		f.Name, f.MIME, f.Size, f.OriginalName).Scan(&id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to create file. No rows returned from id: ", id)
		return -1, rerr.NotFound
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create file %#v", f), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetFile - Gets File from DB
func GetFile(id int64) (*File, *rerr.RestError) {
	file := new(File)

	err := db.DB.Get(file, `SELECT id, name, mime, size, original_name FROM file WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get file. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get file from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return file, nil
}

// GetFiles - Gets File from DB
func GetFiles(fileIDs []int64) ([]File, *rerr.RestError) {
	query, args, err := sqlx.In(`SELECT id, name, mime, size, original_name FROM file WHERE id IN(?)`, fileIDs)
	if err != nil {
		log.Error("Error building query: ", err)
		return nil, rerr.InternalServerError
	}

	files := make([]File, 0)
	query = db.DB.Rebind(query)

	err = db.DB.Select(&files, query, args...)
	if err == sql.ErrNoRows {
		log.Warning("Failed to get file. No rows returned")
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get files: ", err)
		return nil, rerr.InternalServerError
	}

	return files, nil
}

// DeleteFile - Deletes file in DB, returns rerr.RestError
func DeleteFile(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM file WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete file from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete file from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete file. No rows returned from id: ", id)
		return rerr.NotFound
	}

	return nil
}
