package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Image - Clothing Image Struct
type Image struct {
	ID         db.NullInt64 `db:"id, pkey" json:"id"`
	ClothingID db.NullInt64 `db:"clothing_id" json:"clothing_id"`
	ColorID    db.NullInt64 `db:"color_id" json:"color_id"`
	FileID     db.NullInt64 `db:"file_id" json:"file_id"`
	Aspect     db.NullInt64 `db:"aspect" json:"aspect"`
}

// ImageResponse - ImageResponse Struct
type ImageResponse struct {
	ID         db.NullInt64  `db:"id, pkey" json:"id"`
	ClothingID db.NullInt64  `db:"clothing_id" json:"clothing_id"`
	ColorID    db.NullInt64  `db:"color_id" json:"color_id"`
	Aspect     db.NullInt64  `db:"aspect" json:"aspect"`
	Name       db.NullString `db:"name" json:"name"`
}

// CreateImage - Creates Clothing Image
func CreateImage(c *Image) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO image(clothing_id, color_id, file_id, aspect) VALUES ($1, $2, $3, $4) RETURNING id`, c.ClothingID, c.ColorID, c.FileID, c.Aspect).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create Clothing Image. Already exists %#v", c))
		return -1, rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Clothing Image. %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetImage - Gets Clothing Image from id
func GetImage(id int64) (*Image, *rerr.RestError) {
	size := new(Image)

	err := db.DB.Get(size, `SELECT id, clothing_id, color_id, file_id FROM image WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Clothing Image. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Clothing Image from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return size, nil
}

// GetAllImages - Gets all Clothing Images
func GetAllImages(clothingID int64) ([]*ImageResponse, *rerr.RestError) {
	images := make([]*ImageResponse, 0)
	err := db.DB.Select(&images, `SELECT i.id, i.clothing_id, i.color_id, i.aspect, f.name 
															FROM image AS i
															LEFT JOIN file AS f ON f.id = i.file_id
															WHERE i.clothing_id=$1`, clothingID)

	if err != nil {
		log.Error("Failed to get Clothing Images: ", err)
		return nil, rerr.InternalServerError
	}

	return images, nil
}

// UpdateImage - Updates Clothing Image
func UpdateImage(c *Image) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE image SET color_id=$2 
							WHERE id=$1`, c.ID, c.ColorID)

	if err != nil {
		log.Error("Failed to update Clothing Image from id: ", c.ID.Int64, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Clothing Image. %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Clothing Image. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteImage - Deletes Clothing Image from ID
func DeleteImage(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM image WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Clothing Image from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Clothing Image from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Clothing Image. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}
