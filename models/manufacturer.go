package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Manufacturer - Manufacturer Struct
type Manufacturer struct {
	ID   db.NullInt64  `db:"id, pkey" json:"id"`
	Name db.NullString `db:"name" json:"name"`
}

// CreateManufacturer - Creates Manufacturer
func CreateManufacturer(c *Manufacturer) (int64, *rerr.RestError) {
	var id int64

	err := db.DB.QueryRow(`INSERT INTO manufacturer(name) VALUES ($1) RETURNING id`, c.Name).Scan(&id)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Manufacturer %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetManufacturer - Gets Manufacturer by id
func GetManufacturer(id int64) (*Manufacturer, *rerr.RestError) {
	manufacturer := new(Manufacturer)

	err := db.DB.Get(manufacturer, `SELECT id, name FROM manufacturer WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Error("Failed to get Manufacturer. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Manufacturer from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return manufacturer, nil
}

// GetManufacturers - Gets All Manufacturers
func GetManufacturers() ([]Manufacturer, *rerr.RestError) {
	manufacturers := make([]Manufacturer, 0)

	err := db.DB.Select(&manufacturers, `SELECT id, name FROM manufacturer`)

	if err == sql.ErrNoRows {
		log.Error("Failed to get Manufacturers. No rows returned")
		return manufacturers, nil
	}

	if err != nil {
		log.Error("Failed to get Manufacturers: ", err)
		return nil, rerr.InternalServerError
	}

	return manufacturers, nil
}

// UpdateManufacturer - Updates Manufacturer
func UpdateManufacturer(c *Manufacturer) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE manufacturer SET name=$2 WHERE id=$1`, c.ID, c.Name)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Manufacturer %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Manufacturer %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Category. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteManufacturer - Deletes Manufacturer
func DeleteManufacturer(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM manufacturer WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Manufacturer from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if rows < 1 {
		log.Warning("Failed to delete Manufacturer. No rows returned from id: ", id)
		return rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to delete Manufacturer from id: ", id, err)
		return rerr.InternalServerError
	}

	return nil
}
