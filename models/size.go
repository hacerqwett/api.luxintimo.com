package models

import (
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Size - Size Struct
type Size struct {
	ID         db.NullInt64 `db:"id, pkey" json:"id"`
	ClothingID db.NullInt64 `db:"clothing_id" json:"clothing_id"`
	ColorID    db.NullInt64 `db:"color_id" json:"color_id"`
	Size       db.NullInt64 `db:"size" json:"size"`
	Count      db.NullInt64 `db:"count" json:"count"`
}

// CreateSize - Creates Size
func CreateSize(c *Size) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO size(clothing_id, color_id, size, count) VALUES ($1, $2, $3, $4) RETURNING id`, c.ClothingID, c.ColorID, c.Size, c.Count).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create Size. Already exists %#v", c))
		return -1, rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Size. %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// UpdateSize - Updates Size
func UpdateSize(c *Size) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE size SET clothing_id=$2, color_id=$3, size=$4, count=$5 WHERE id=$1`, c.ID, c.ClothingID, c.ColorID, c.Size, c.Count)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Size %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Size %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Size. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// DeleteSize - Deletes Size from ID
func DeleteSize(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM size WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Size from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Size from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Size. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}

// GetSizes - Gets Clothing Sizes
func GetSizes(clothingID int64) ([]*Size, *rerr.RestError) {
	sizes := make([]*Size, 0)
	err := db.DB.Select(&sizes, `SELECT s.id, s.clothing_id, s.color_id, s.size, s.count 
															FROM size AS s
															LEFT JOIN clothing AS c ON c.id = s.clothing_id
															WHERE s.clothing_id=$1`, clothingID)

	if err != nil {
		log.Error("Failed to get Clothing Sizes: ", err)
		return nil, rerr.InternalServerError
	}

	return sizes, nil
}
