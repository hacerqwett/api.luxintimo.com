package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// SubCategory - SubCategory Struct
type SubCategory struct {
	ID         db.NullInt64  `db:"id, pkey" json:"id"`
	Name       db.NullString `db:"name" json:"name"`
	CategoryID db.NullInt64  `db:"category_id" json:"category_id"`
}

// CreateSubCategory - Creates SubCategory
func CreateSubCategory(c *SubCategory) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO subcategory(name, category_id) VALUES ($1, $2) RETURNING id`, c.Name, c.CategoryID).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning(fmt.Sprintf("Failed to create SubCategory. Already exists %#v", c), err)
		return -1, rerr.AlreadyExists
	}

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create SubCategory. %#v", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetSubCategory - Gets SubCategory from id
func GetSubCategory(id int64) (*SubCategory, *rerr.RestError) {
	SubCategory := new(SubCategory)

	err := db.DB.Get(SubCategory, `SELECT id, name, category_id FROM subcategory WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get SubCategory. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Categories from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return SubCategory, nil
}

// GetSubCategories - Gets all SubCategories
func GetSubCategories() ([]SubCategory, *rerr.RestError) {
	categories := make([]SubCategory, 0)
	err := db.DB.Select(&categories, `SELECT id, name, category_id FROM subcategory`)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Categories. No rows returned.")
		return categories, nil
	}

	if err != nil {
		log.Error("Failed to get Categories: ", err)
		return nil, rerr.InternalServerError
	}

	return categories, nil
}

// UpdateSubCategory - Updates SubCategory
func UpdateSubCategory(c *SubCategory) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE subcategory SET name=$2, category_id=$3 WHERE id=$1`, c.ID, c.Name, c.CategoryID)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update SubCategory %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update SubCategory %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update SubCategory. No rows returned from id: ", c.ID.Int64)
		return rerr.SubCategoryNotFound
	}

	return nil
}

// DeleteSubCategory - Deletes SubCategory
func DeleteSubCategory(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM subcategory WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to get Categories from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to get Categories from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Error("Failed to get Categories. No rows returned from id: ", id)
		return rerr.SubCategoryNotFound
	}

	return nil
}
