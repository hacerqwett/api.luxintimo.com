package models

import (
	"database/sql"
	"net/http"

	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/auth"
	"api.luxintimo.com/utils/utime"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
)

// User - User Model
type User struct {
	ID         db.NullInt64  `db:"id, pkey" json:"id"`
	Email      db.NullString `db:"email" json:"email"`
	Name       db.NullString `db:"name" json:"name"`
	Password   db.NullString `db:"password" json:"-"`
	Address    db.NullString `db:"address" json:"address"`
	Phone      db.NullString `db:"phone" json:"phone"`
	Activated  db.NullBool   `db:"activated" json:"activated"`
	Admin      db.NullBool   `db:"admin" json:"admin"`
	LastLogin  db.NullString `db:"last_login" json:"last_login"`
	DateJoined db.NullString `db:"date_joined" json:"date_joined"`
}

// ValidateActivated - Checks user.Activated, on error returns rerr.UserNotActivated
func (u *User) ValidateActivated() *rerr.RestError {
	if !u.Activated.IsValidAndTrue() {
		return rerr.UserNotActivated
	}

	return nil
}

// ValidateAdmin - Checks user.Admin, on error returns rerr.UserNotActivated
func (u *User) ValidateAdmin() *rerr.RestError {
	if !u.Admin.IsValidAndTrue() {
		return rerr.Unauthorized
	}

	return nil
}

// RegisterUser - Register new User, returns registered User.
func RegisterUser(u *User) (int64, *rerr.RestError) {
	var id int64 = -1

	err := db.DB.QueryRow(`INSERT INTO users(email, name, password, address, phone)
							VALUES ($1, $2, $3, $4, $5) RETURNING id`,
		u.Email, u.Name, u.Password, u.Address, u.Phone).Scan(&id)

	if rerr.IsAlreadyExists(err) {
		log.Warning("Failed to register user. Already exists ", u, err)
		return -1, rerr.InternalServerError
	}

	if err != nil {
		log.Error("Error registering user: ", err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetUser - Gets User by ID, returns User struct
func GetUser(id int64) (*User, *rerr.RestError) {
	user := new(User)

	err := db.DB.Get(user, `SELECT id, email, name, password, address, phone, activated, admin, last_login, date_joined 
							FROM users WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		return nil, rerr.InvalidEmailOrPassword
	}

	if err != nil {
		log.Error("Error getting user by id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return user, nil
}

// GetUserByEmail - Gets User by Email, returns User struct
func GetUserByEmail(email string) (*User, *rerr.RestError) {
	user := new(User)

	err := db.DB.Get(user, `SELECT id, email, name, password, address, phone, activated, admin, last_login, date_joined 
							FROM users WHERE email=$1`, email)

	if err == sql.ErrNoRows {
		log.Warning("Error getting user. No rows returned from email: ", email, err)
		return nil, rerr.InvalidEmailOrPassword
	}

	if err != nil {
		log.Error("Error getting user by email: ", email, err)
		return nil, rerr.InternalServerError
	}

	return user, nil
}

// GetUserFromHeader - Gets User by Header JWT, returns User struct
func GetUserFromHeader(r *http.Request) (*User, *rerr.RestError) {
	id, idErr := auth.JWTGetIDFromHeader(r)
	if idErr != nil {
		return nil, idErr
	}

	user := new(User)
	err := db.DB.Get(user, `SELECT id, email, name, password, address, phone, activated, admin, last_login, date_joined 
							FROM users WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Error getting user by Header JWT. No rows returned from id: ", id)
		return nil, rerr.Unauthorized
	}

	if err != nil {
		log.Error("Error getting user from Header JWT id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return user, nil
}

// UpdateUserLastLogin - Updates User last Login
func UpdateUserLastLogin(id int64) (string, *rerr.RestError) {
	var t string

	err := db.DB.QueryRow(`UPDATE users SET last_login=$1 WHERE id=$2 
							RETURNING last_login`, utime.GetTime(), id).Scan(&t)

	if err == sql.ErrNoRows {
		log.Warning("Error updating user last_login. No rows returned from id: ", id)
		return "", rerr.UnknownRequestStructure
	}

	if err != nil {
		log.Error("Error updating user last_login from id: ", id, err)
		return "", rerr.InternalServerError
	}

	return t, nil
}
