package models

import (
	"database/sql"
	"fmt"

	"api.luxintimo.com/db"
	"api.luxintimo.com/log"
	rerr "api.luxintimo.com/rerr"
)

// Views - Views Struct
type Views struct {
	ID         db.NullInt64 `db:"id, pkey" json:"id"`
	ClothingID db.NullInt64 `db:"clothing_id" json:"clothing_id"`
	UserID     db.NullInt64 `db:"user_id" json:"user_id"`
	Views      db.NullInt64 `db:"views" json:"views"`
}

// CreateViews - Creates Views
func CreateViews(c *Views) (int64, *rerr.RestError) {
	var id int64
	err := db.DB.QueryRow(`INSERT INTO comment(clothing_id, user_id, comment) VALUES ($1, $2, $3) RETURNING id`, c.ClothingID, c.UserID, c.Views).Scan(&id)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to create Views. %#v: ", c), err)
		return -1, rerr.InternalServerError
	}

	return id, nil
}

// GetViews - Gets Views from id
func GetViews(id int64) (*Views, *rerr.RestError) {
	size := new(Views)

	err := db.DB.Get(size, `SELECT id, clothing_id, user_id, comment FROM comment WHERE id=$1`, id)

	if err == sql.ErrNoRows {
		log.Warning("Failed to get Views. No rows returned from id: ", id)
		return nil, rerr.NotFound
	}

	if err != nil {
		log.Error("Failed to get Views from id: ", id, err)
		return nil, rerr.InternalServerError
	}

	return size, nil
}

// UpdateViews - Updates Views
func UpdateViews(c *Views) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE views SET clothing_id=$2, user_id=$3, views=$4 WHERE id=$1`, c.ID, c.ClothingID, c.UserID, c.Views)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Views %#v", c), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Views %#v", c), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to update Views. No rows returned from id: ", c.ID.Int64)
		return rerr.NotFound
	}

	return nil
}

// IncrementViews - Increment Views
func IncrementViews(userID int64, clothingID int64) *rerr.RestError {
	res, err := db.DB.Exec(`UPDATE views SET views=views + 1 WHERE user_id=$1 AND clothing_id=$2`, userID, clothingID)

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Views. UserID: %d, ClothingID: %d. ", userID, clothingID), err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error(fmt.Sprintf("Failed to update Views. UserID: %d, ClothingID: %d. ", userID, clothingID), err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning(fmt.Sprintf("Failed to update Views. No rows returned. UserID: %d, ClothingID: %d. ", userID, clothingID))
		return rerr.NotFound
	}

	return nil
}

// DeleteViews - Deletes Views from ID
func DeleteViews(id int64) *rerr.RestError {
	res, err := db.DB.Exec(`DELETE FROM views WHERE id=$1`, id)

	if err != nil {
		log.Error("Failed to delete Views from id: ", id, err)
		return rerr.InternalServerError
	}

	rows, err := res.RowsAffected()

	if err != nil {
		log.Error("Failed to delete Views from id: ", id, err)
		return rerr.InternalServerError
	}

	if rows < 1 {
		log.Warning("Failed to delete Views. No rows returned from id: ", err)
		return rerr.ClothingNotFound
	}

	return nil
}
