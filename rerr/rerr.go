package rerr

import (
	"encoding/json"

	"api.luxintimo.com/log"
	"github.com/lib/pq"
)

// RestError uhttp Error Struct
type RestError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Extra   string `json:"extra,omitempty"`
}

// ToJSON Marshal uhttp JSON Error
func (e *RestError) ToJSON() []byte {
	js, err := json.Marshal(e)
	if err != nil {
		log.Error("Couldn't Marshal RestError to JSON", err)
		return []byte{}
	}
	return js
}

var (
	// UnknownRequestStructure UnknownRequestStructure
	UnknownRequestStructure = &RestError{Code: 400, Message: "Request structure cannot be recognized!"}
	// InternalServerError InternalServerError
	InternalServerError = &RestError{Code: 500, Message: "Internal server error!"}

	// NameRequired NameRequired
	NameRequired = &RestError{Code: 400, Message: "Name required!"}
	// EmailRequired EmailRequired
	EmailRequired = &RestError{Code: 400, Message: "Email required!"}
	// AddressRequired AddressRequired
	AddressRequired = &RestError{Code: 400, Message: "Address required!"}
	// PasswordRequired PasswordRequired
	PasswordRequired = &RestError{Code: 400, Message: "Password not provided!"}
	// Password1Required Password1Required
	Password1Required = &RestError{Code: 400, Message: "Password 1 not provided!"}
	// Password2Required Password2Required
	Password2Required = &RestError{Code: 400, Message: "Password 2 not provided!"}
	// PasswordsDontMatch PasswordsDontMatch
	PasswordsDontMatch = &RestError{Code: 400, Message: "The two passwords don't match!"}
	// UserAlreadyExists UserAlreadyExists
	UserAlreadyExists = &RestError{Code: 400, Message: "User with such email already exists!"}

	// InvalidEmailOrPassword InvalidEmailOrPassword
	InvalidEmailOrPassword = &RestError{Code: 401, Message: "Invalid email and/or password!"}

	// NotFound NotFound
	NotFound = &RestError{Code: 404, Message: "Whoops! Something went wrong!"}
	// UserNotFound UserNotFound
	UserNotFound = &RestError{Code: 404, Message: "User not found!"}
	// ClothingNotFound ClothingNotFound
	ClothingNotFound = &RestError{Code: 404, Message: "Clothing not found!"}
	// SizeNotFound SizeNotFound
	SizeNotFound = &RestError{Code: 404, Message: "Size not found!"}
	// CategoryNotFound CategoryNotFound
	CategoryNotFound = &RestError{Code: 404, Message: "Category not found!"}
	// SubCategoryNotFound SubCategoryNotFound
	SubCategoryNotFound = &RestError{Code: 404, Message: "Subcategory not found!"}

	// InvalidSize InvalidSize
	InvalidSize = &RestError{Code: 400, Message: "Invalid Size!"}
	// InvalidClothing InvalidClothing
	InvalidClothing = &RestError{Code: 400, Message: "Invalid Clothing!"}
	// InvalidCategory InvalidCategory
	InvalidCategory = &RestError{Code: 400, Message: "Invalid Category!"}
	// InvalidSubCategory InvalidSubCategory
	InvalidSubCategory = &RestError{Code: 400, Message: "Invalid Subcategory!"}
	// InvalidManufacturer InvalidManufacturer
	InvalidManufacturer = &RestError{Code: 400, Message: "Invalid Manufacturer!"}
	// InvalidDescription InvalidDescription
	InvalidDescription = &RestError{Code: 400, Message: "Invalid Description!"}
	// InvalidGender InvalidGender
	InvalidGender = &RestError{Code: 400, Message: "Invalid Gender!"}
	// InvalidColor InvalidColor
	InvalidColor = &RestError{Code: 400, Message: "Invalid Color!"}
	// InvalidPrice InvalidPrice
	InvalidPrice = &RestError{Code: 400, Message: "Invalid Price!"}
	// InvalidDiscount InvalidDiscount
	InvalidDiscount = &RestError{Code: 400, Message: "Invalid Discount!"}
	// InvalidStatus InvalidStatus
	InvalidStatus = &RestError{Code: 400, Message: "Invalid Status!"}
	// InvalidToken InvalidToken
	InvalidToken = &RestError{Code: 400, Message: "Invalid Token provided!"}
	// InvalidAmount InvalidAmount
	InvalidAmount = &RestError{Code: 400, Message: "Invalid Amount provided!"}
	// InvalidMinPrice InvalidMinPrice
	InvalidMinPrice = &RestError{Code: 400, Message: "Invalid Minimum Price provided!"}
	// InvalidMaxPrice InvalidMaxPrice
	InvalidMaxPrice = &RestError{Code: 400, Message: "Invalid Maximum Price provided!"}
	// MinPriceBiggerThanMax MinPriceBiggerThanMax
	MinPriceBiggerThanMax = &RestError{Code: 400, Message: "Minimum Price is bigger than Maximum Price!"}

	// AlreadyExists AlreadyExists
	AlreadyExists = &RestError{Code: 400, Message: "Item already exists!"}

	// FailedToCreateActivation FailedToCreateActivation
	FailedToCreateActivation = &RestError{Code: 500, Message: "Failed to create activation!"}
	// FailedToActivateUser FailedToActivateUser
	FailedToActivateUser = &RestError{Code: 500, Message: "Failed to activate user!"}
	// SendActivationUserEmailError SendActivationUserEmailError
	SendActivationUserEmailError = &RestError{Code: 500, Message: "Failed to send activation email!"}
	// FailedToGenerateActivationKey FailedToGenerateActivationKey
	FailedToGenerateActivationKey = &RestError{Code: 500, Message: "Failed to generate activation key!"}
	// InvalidActivationKey InvalidActivationKey
	InvalidActivationKey = &RestError{Code: 500, Message: "Invalid activation key!"}

	// InvalidFile InvalidFile
	InvalidFile = &RestError{Code: 400, Message: "Invalid file!"}
	// InvalidMIMEType InvalidMIMEType
	InvalidMIMEType = &RestError{Code: 400, Message: "Invalid file type!"}
	// ExceedMaxFileSize ExceedMaxFileSize
	ExceedMaxFileSize = &RestError{Code: 400, Message: "Exceed max file size!"}

	// UserNotActivated UserNotActivated
	UserNotActivated = &RestError{Code: 400, Message: "User not activated!"}
	// Unauthorized Unauthorized
	Unauthorized = &RestError{Code: 400, Message: "Unathorized!"}
)

// IsAlreadyExists - Checks if error is already Exists error
func IsAlreadyExists(err error) bool {
	pqe, ok := err.(*pq.Error)
	if ok != true {
		return false
	}

	if string(pqe.Code) == "23505" {
		return true
	}

	return false
}
