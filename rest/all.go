package rest

import "api.luxintimo.com/models"

// AllResponse - AllResponse Struct
type AllResponse struct {
	Categories    []models.Category     `json:"categories"`
	SubCategories []models.SubCategory  `json:"subcategories"`
	Manufacturers []models.Manufacturer `json:"manufacturers"`
	Colors        []models.Color        `json:"colors"`
}

// AdminAllResponse - AdminAllResponse Struct
type AdminAllResponse struct {
	Clothing []models.Clothing `json:"clothing"`
}
