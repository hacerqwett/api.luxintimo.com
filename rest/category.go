package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/constants"
)

// CategoryRequest - CategoryRequest Struct
type CategoryRequest struct {
	Name   db.NullString `json:"name"`
	Gender db.NullInt64  `json:"gender"`
}

// Validate - returns rerr.RestError if invalid
func (r *CategoryRequest) Validate() *rerr.RestError {
	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	if !r.Gender.IsValid() || !constants.ValidGender(r.Gender.Int64) {
		return rerr.InvalidGender
	}

	return nil
}
