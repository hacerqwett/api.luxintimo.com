package rest

import (
	"strconv"

	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/constants"
)

// ClothingRequest - ClothingRequest Struct
type ClothingRequest struct {
	Name           db.NullString  `json:"name"`
	ManufacturerID db.NullInt64   `json:"manufacturer_id"`
	SubCategoryID  db.NullInt64   `json:"subcategory_id"`
	Status         db.NullInt64   `json:"status"`
	Price          db.NullFloat64 `json:"price"`
	Discount       db.NullFloat64 `json:"discount"`
	Description    db.NullString  `json:"description,omitempty"`
}

// FilterParamsRequest - FilterParamsRequest Struct
type FilterParamsRequest struct {
	Category      string   `json:"category"`
	Gender        string   `json:"gender"`
	SubCategories []string `json:"subCategories"`
	Sizes         []string `json:"sizes"`
	Colors        []string `json:"colors"`
	Manufacturers []string `json:"manufacturers"`
	Sort          string   `json:"sort"`
	MinPrice      string   `json:"minPrice"`
	MaxPrice      string   `json:"maxPrice"`
}

// Validate - returns rerr.RestError if invalid
func (r *ClothingRequest) Validate() *rerr.RestError {
	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	if !r.ManufacturerID.IsValid() {
		return rerr.InvalidManufacturer
	}

	if !r.SubCategoryID.IsValid() {
		return rerr.InvalidSubCategory
	}

	if !r.Price.IsValid() {
		return rerr.InvalidPrice
	}

	if !r.Discount.IsValid() {
		return rerr.InvalidDiscount
	}

	if !r.Description.IsValid() {
		return rerr.InvalidDescription
	}

	return nil
}

// Validate - returns rerr.RestError if invalid
func (r *FilterParamsRequest) Validate() *rerr.RestError {
	gender, err := strconv.ParseInt(r.Gender, 10, 64)

	if err != nil {
		return rerr.InvalidGender
	}

	if !constants.ValidGender(gender) {
		return rerr.InvalidGender
	}

	if len(r.SubCategories) > 0 {
		for _, subCategory := range r.SubCategories {
			if subCategory == "" {
				return rerr.InvalidSubCategory
			}

			_, err := strconv.ParseInt(r.Gender, 10, 64)
			if err != nil {
				return rerr.InvalidSubCategory
			}
		}
	}

	if len(r.Sizes) > 0 {
		for _, size := range r.Sizes {
			s, err := strconv.ParseInt(size, 10, 64)
			if err != nil {
				return rerr.InvalidSize
			}

			if !constants.ValidSize(s) {
				return rerr.InvalidSize
			}
		}
	}

	if len(r.Colors) > 0 {
		for _, color := range r.Colors {
			if color == "" {
				return rerr.InvalidColor
			}

			_, err := strconv.ParseInt(color, 10, 64)
			if err != nil {
				return rerr.InvalidColor
			}
		}
	}

	if len(r.Manufacturers) > 0 {
		for _, manufacturer := range r.Manufacturers {
			if manufacturer == "" {
				return rerr.InvalidManufacturer
			}

			_, err := strconv.ParseInt(manufacturer, 10, 64)
			if err != nil {
				return rerr.InvalidManufacturer
			}
		}
	}

	// if !r.Sort.IsValid() {
	// 	return rerr.InvalidDiscount
	// }

	if r.MinPrice == "" && r.MaxPrice == "" {
		return nil
	}

	if r.MinPrice == "" && r.MaxPrice != "" {
		return rerr.InvalidMinPrice
	}

	if r.MinPrice != "" && r.MaxPrice == "" {
		return rerr.InvalidMaxPrice
	}

	minPrice, err := strconv.ParseFloat(r.MinPrice, 64)
	if minPrice < 0 {
		return rerr.InvalidMinPrice
	}

	maxPrice, err := strconv.ParseFloat(r.MaxPrice, 64)
	if maxPrice < 0 {
		return rerr.InvalidMaxPrice
	}

	if minPrice > maxPrice {
		return rerr.MinPriceBiggerThanMax
	}

	return nil
}
