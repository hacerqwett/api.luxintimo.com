package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
)

// ClothingDetailsRequest - ClothingDetailsRequest Struct
type ClothingDetailsRequest struct {
	ClothingID  db.NullInt64  `json:"clothing_id"`
	Description db.NullString `json:"description"`
}

// Validate - returns rerr.RestError if invalid
func (r *ClothingDetailsRequest) Validate() *rerr.RestError {
	if !r.ClothingID.IsValid() {
		return rerr.InvalidClothing
	}

	if !r.Description.IsValid() {
		return rerr.InvalidDescription
	}

	return nil
}
