package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/constants"
)

// ColorRequest - ColorRequest Struct
type ColorRequest struct {
	Name db.NullString `json:"name"`
	HEX  db.NullString `json:"hex"`
}

// Validate - returns rerr.RestError if invalid
func (r *ColorRequest) Validate() *rerr.RestError {
	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	if !r.HEX.IsValid() || !constants.ValidHEX(r.HEX.String) {
		return rerr.InvalidColor
	}

	return nil
}
