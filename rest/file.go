package rest

// UploadFileResponse - UploadFileResponse Struct
type UploadFileResponse struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
