package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/constants"
)

// UploadImageResponse - UploadImageResponse Struct
type UploadImageResponse struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	Aspect int64  `json:"aspect"`
}

// UploadImageRequest - UploadImageRequest Struct
type UploadImageRequest struct {
	ClothingID db.NullInt64 `json:"clothing_id"`
	Aspect     db.NullInt64 `json:"aspect"`
}

// Validate - returns rerr.RestError if invalid
func (r *UploadImageRequest) Validate() *rerr.RestError {
	if !r.ClothingID.IsValid() {
		return rerr.InvalidClothing
	}

	if !r.Aspect.IsValid() || !constants.ValidAspect(r.Aspect.Int64) {
		return rerr.InternalServerError
	}

	return nil
}

// UpdateImageRequest - UpdateImageRequest Struct
type UpdateImageRequest struct {
	ColorID db.NullInt64 `json:"color_id"`
}

// Validate - returns rerr.RestError if invalid
func (r *UpdateImageRequest) Validate() *rerr.RestError {
	if r.ColorID.Valid && r.ColorID.Int64 < 1 {
		return rerr.InvalidColor
	}

	return nil
}
