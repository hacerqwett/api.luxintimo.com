package rest

import (
	"api.luxintimo.com/db"
	"api.luxintimo.com/models"
	rerr "api.luxintimo.com/rerr"
)

// LoginUserRequest - Struct of Login User Request
type LoginUserRequest struct {
	Email    db.NullString `json:"email"`
	Password db.NullString `json:"password"`
}

// LoginUserResponse - Struct of Login User Response
type LoginUserResponse struct {
	User  *models.User `json:"user"`
	Token string       `json:"token"`
}

// Validate - Returns rerr.RestError if Request not Valid
func (r *LoginUserRequest) Validate() *rerr.RestError {
	if !r.Email.IsValid() {
		return rerr.EmailRequired
	}

	if !r.Password.IsValid() {
		return rerr.PasswordRequired
	}

	return nil
}
