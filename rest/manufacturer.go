package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
)

// ManufacturerRequest - ManufacturerRequest Struct
type ManufacturerRequest struct {
	Name db.NullString `json:"name"`
}

// Validate - returns rerr.RestError if invalid
func (r *ManufacturerRequest) Validate() *rerr.RestError {
	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	return nil
}
