package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
)

// RegisterUserRequest - Struct of Register User Request
type RegisterUserRequest struct {
	Email     db.NullString `json:"email"`
	Name      db.NullString `json:"name"`
	Password1 db.NullString `json:"password1"`
	Password2 db.NullString `json:"password2"`
	Address   db.NullString `json:"address"`
	Phone     db.NullString `json:"phone"`
}

// Validate - Returns rerr.RestError if Request not Valid
func (r *RegisterUserRequest) Validate() *rerr.RestError {
	if !r.Email.IsValid() {
		return rerr.EmailRequired
	}

	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	if !r.Password1.IsValid() {
		return rerr.Password1Required
	}

	if !r.Password2.IsValid() {
		return rerr.Password2Required
	}

	if r.Password1.String != r.Password2.String {
		return rerr.PasswordsDontMatch
	}

	if !r.Address.IsValid() {
		return rerr.AddressRequired
	}

	return nil
}
