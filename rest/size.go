package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/constants"
)

// SizeRequest - SizeRequest Struct
type SizeRequest struct {
	ClothingID db.NullInt64 `json:"clothing_id"`
	ColorID    db.NullInt64 `json:"color_id"`
	Count      db.NullInt64 `json:"count"`
	Size       db.NullInt64 `json:"size"`
}

// Validate - returns rerr.RestError if invalid
func (r *SizeRequest) Validate() *rerr.RestError {
	if !r.ClothingID.IsValid() {
		return rerr.InvalidClothing
	}

	if !r.ColorID.IsValid() {
		return rerr.InvalidClothing
	}

	if !r.Count.IsValid() {
		return rerr.InvalidAmount
	}

	if !r.Size.IsValid() || !constants.ValidSize(r.Size.Int64) {
		return rerr.InvalidSize
	}

	return nil
}
