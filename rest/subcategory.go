package rest

import (
	"api.luxintimo.com/db"
	rerr "api.luxintimo.com/rerr"
)

// SubCategoryRequest - SubCategoryRequest Struct
type SubCategoryRequest struct {
	Name       db.NullString `json:"name"`
	CategoryID db.NullInt64  `json:"category_id"`
}

// Validate - returns rerr.RestError if invalid
func (r *SubCategoryRequest) Validate() *rerr.RestError {
	if !r.Name.IsValid() {
		return rerr.NameRequired
	}

	if !r.CategoryID.IsValid() {
		return rerr.InvalidCategory
	}

	return nil
}
