DROP DATABASE IF EXISTS luxdb;
CREATE DATABASE luxdb WITH TEMPLATE = template0 ENCODING = 'UTF8';

\c luxdb

CREATE TABLE "users" (
    id bigserial PRIMARY KEY,
    email varchar(250) UNIQUE NOT NULL,
    name varchar(250) NOT NULL,
    password varchar(250) NOT NULL,
    address varchar(250) NOT NULL,
    phone varchar(250),
    activated boolean DEFAULT false,
    admin boolean DEFAULT false,
    last_login TIMESTAMP WITHOUT TIME ZONE,
    date_joined TIMESTAMP DEFAULT (now() AT TIME ZONE 'utc')
);

CREATE TABLE "activation" (
    id bigserial PRIMARY KEY,
    date_sent TIMESTAMP DEFAULT (now() AT TIME ZONE 'utc'),
    date_activated TIMESTAMP WITHOUT TIME ZONE,
    key varchar(250) NOT NULL,
    user_id bigint UNIQUE NOT NULL,

    FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "category" (
    id bigserial PRIMARY KEY,
    name varchar(250) NOT NULL,
    gender smallint NOT NULL,
    UNIQUE (name, gender)
);

CREATE TABLE "subcategory" (
    id bigserial PRIMARY KEY,
    name varchar(250) NOT NULL,
    category_id bigint NOT NULL,
    UNIQUE (name, category_id),

    FOREIGN KEY (category_id) REFERENCES "category" (id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE "manufacturer" (
    id bigserial PRIMARY KEY,
    name varchar(250) UNIQUE NOT NULL
);

CREATE TABLE "clothing" (
    id bigserial PRIMARY KEY,
    name varchar(250),
    status smallint DEFAULT 1,
    price decimal(12,2),
    discount decimal(12,2),
    manufacturer_id bigint,
    subcategory_id bigint,

    FOREIGN KEY (subcategory_id) REFERENCES "subcategory" (id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (manufacturer_id) REFERENCES "manufacturer" (id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE "clothing_details" (
    clothing_id bigint UNIQUE NOT NULL,
    description varchar(250),

    PRIMARY KEY(clothing_id),
    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "color" (
    id bigserial PRIMARY KEY,
    name varchar(100) NOT NULL,
    hex varchar(7) UNIQUE CHECK (char_length(hex) = 7) NOT NULL
);

CREATE TABLE "file" (
    id bigserial PRIMARY KEY,
    name varchar(100) NOT NULL,
    original_name varchar(250) NOT NULL,
    mime smallint NOT NULL,
    size bigint NOT NULL,
    date_created TIMESTAMP DEFAULT (now() AT TIME ZONE 'utc')
);

CREATE TABLE "image" (
    id bigserial PRIMARY KEY,
    clothing_id bigint NOT NULL,
    color_id bigint,
    file_id bigint UNIQUE NOT NULL,
    aspect smallint NOT NULL,

    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (color_id) REFERENCES "color" (id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (file_id) REFERENCES "file" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "size" (
    id bigserial PRIMARY KEY,
    clothing_id bigint NOT NULL,
    color_id bigint NOT NULL,
    size smallint NOT NULL,
    count bigint NOT NULL,
 
    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (color_id) REFERENCES "color" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "comment" (
    id bigserial PRIMARY KEY,
    clothing_id bigint NOT NULL,
    user_id bigint NOT NULL,
    comment varchar(500),

    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "views" (
    id bigserial PRIMARY KEY,
    views bigint DEFAULT 0,
    clothing_id bigint NOT NULL,
    user_id bigint NOT NULL,

    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "like" (
    id bigserial PRIMARY KEY,
    clothing_id bigint NOT NULL,
    user_id bigint NOT NULL,

    FOREIGN KEY (clothing_id) REFERENCES "clothing" (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE CASCADE ON UPDATE CASCADE
);
