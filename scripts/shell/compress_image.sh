#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Enter file name and aspect ratio (2 for 1:1.2, default 1 for 1:1)'
    exit 1
fi

sizes75=("360x480" "540x720" "720x960" "900x1200" "1080x1440" "1296x1728" "1512x2016" "1728x2304" "2049x2732")
sizes1=("2048" "1728" "1512" "1296" "1080" "900" "720" "540" "360" "180")

sizes=() 
if [ $2 = "2" ]; then
	sizes=("${sizes75[@]}")    
else
	sizes=("${sizes1[@]}")    
fi

for s in ${sizes[@]}
do 
	size=${s}
	out=${s}"-"$1
	temp="temp-"$out
	convert $1 -resize $size $out
	../cjpeg -quality 70 $out > $temp
	rm $out
	mv $temp $out
done