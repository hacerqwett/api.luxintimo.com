#!/bin/bash

openssl genrsa -out ../../certs/jwt/private.pem 4096
openssl rsa -in ../../certs/jwt/private.pem -pubout > ../../certs/jwt/public.pem