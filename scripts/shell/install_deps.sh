sudo apt-get -y install cmake autoconf automake libtool nasm make pkg-config git
sudo apt-get -y install imagemagick

git clone https://github.com/mozilla/mozjpeg.git

cd mozjpeg
mkdir build && cd build
sudo cmake -G"Unix Makefiles" ../
sudo make install

cd ../..

sudo ln -s /opt/mozjpeg/bin/cjpeg .

sudo rm -rf mozjpeg