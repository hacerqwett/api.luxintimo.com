package server

import (
	"net/http"

	rerr "api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/uhttp"

	"api.luxintimo.com/utils/auth"
)

// CORSHandler Writes CORS to the response
func CORSHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if origin := r.Header.Get("Origin"); origin != "" {
			rw.Header().Set("Access-Control-Allow-Origin", origin)
			rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
			rw.Header().Set("Access-Control-Allow-Headers",
				"Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")
		}
		// Stop here if its Preflighted OPTIONS request
		if r.Method == "OPTIONS" {
			return
		}
		h.ServeHTTP(rw, r)
	})
}

// ValidateJWTHandler - Validates JWT Handler
func ValidateJWTHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := auth.JWTValidateHeader(r); err != nil {
			uhttp.WriteError(w, rerr.Unauthorized)
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

// CloseBodyHandler - Close Body Handler
func CloseBodyHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
		r.Body.Close()
	})
}
