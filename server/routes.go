package server

import (
	"net/http"

	"api.luxintimo.com/controllers/admin"
	"api.luxintimo.com/controllers/clothing"
	"api.luxintimo.com/controllers/user"
)

// Route HTTP Route Structure
type Route struct {
	url     string
	auth    bool
	handler http.HandlerFunc
	methods []string
}

// Routes HTTP Routes
var Routes = []*Route{
	// Users
	{url: "/api/register", auth: false, handler: user.RegisterUserHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/activate/{key}", auth: false, handler: user.ActivateUserHandler, methods: []string{"GET", "OPTIONS"}},
	{url: "/api/login", auth: false, handler: user.LoginUserHandler, methods: []string{"POST", "OPTIONS"}},

	// Clothing
	{url: "/api/all", auth: false, handler: clothing.GetAllHandler, methods: []string{"GET", "OPTIONS"}},
	{url: "/api/clothing/{id}", auth: false, handler: clothing.GetClothingHandler, methods: []string{"GET", "OPTIONS"}},
	{url: "/api/collection/{index}", auth: false, handler: clothing.GetFilteredClothingHandler, methods: []string{"GET", "OPTIONS"}},

	// Admin
	{url: "/api/clothing", auth: true, handler: admin.CreateClothingHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/clothing/{id}", auth: true, handler: admin.UpdateClothingHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/clothing/{id}", auth: true, handler: admin.DeleteClothingHandler, methods: []string{"DELETE", "OPTIONS"}},

	{url: "/api/manufacturer", auth: true, handler: admin.CreateManufacturerHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/manufacturer/{id}", auth: true, handler: admin.UpdateManufacturerHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/manufacturer/{id}", auth: true, handler: admin.DeleteManufacturerHandler, methods: []string{"DELETE", "OPTIONS"}},

	{url: "/api/category", auth: true, handler: admin.CreateCategoryHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/category/{id}", auth: true, handler: admin.UpdateCategoryHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/category/{id}", auth: true, handler: admin.DeleteCategoryHandler, methods: []string{"DELETE", "OPTIONS"}},

	{url: "/api/subcategory", auth: true, handler: admin.CreateSubCategoryHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/subcategory/{id}", auth: true, handler: admin.UpdateSubCategoryHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/subcategory/{id}", auth: true, handler: admin.DeleteSubCategoryHandler, methods: []string{"DELETE", "OPTIONS"}},

	{url: "/api/size", auth: true, handler: admin.CreateSizeHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/size/{id}", auth: true, handler: admin.UpdateSizeHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/size/{id}", auth: true, handler: admin.DeleteSizeHandler, methods: []string{"DELETE", "OPTIONS"}},

	{url: "/api/color", auth: true, handler: admin.CreateColorHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/color/{id}", auth: true, handler: admin.UpdateColorHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/color/{id}", auth: true, handler: admin.DeleteColorHandler, methods: []string{"DELETE", "OPTIONS"}},

	// Files
	{url: "/api/file", auth: true, handler: admin.UploadFileHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/file/{id}", auth: true, handler: admin.DeleteFileHandler, methods: []string{"DELETE", "OPTIONS"}},

	// Images
	{url: "/api/image", auth: true, handler: admin.UploadImageHandler, methods: []string{"POST", "OPTIONS"}},
	{url: "/api/image/{id}", auth: true, handler: admin.UpdateImageHandler, methods: []string{"PATCH", "OPTIONS"}},
	{url: "/api/image/{id}", auth: true, handler: admin.DeleteImageHandler, methods: []string{"DELETE", "OPTIONS"}},
}
