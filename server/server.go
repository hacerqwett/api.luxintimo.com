package server

import (
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/justinas/alice"

	"api.luxintimo.com/config"
	"api.luxintimo.com/log"
	"github.com/gorilla/mux"
)

// Start HTTP Server
func Start() error {
	router := mux.NewRouter()

	commonHandlers := alice.New(CORSHandler, CloseBodyHandler, handlers.CompressHandler, handlers.RecoveryHandler())
	authHandlers := commonHandlers.Append(ValidateJWTHandler)

	for _, r := range Routes {
		if r.auth {
			router.Handle(r.url, authHandlers.Then(r.handler)).Methods(r.methods...)
		} else {
			router.Handle(r.url, commonHandlers.Then(r.handler)).Methods(r.methods...)
		}
	}

	path := config.Config.StaticFilesPath
	endpoint := config.Config.StaticFilesEndpoint

	fs := http.FileServer(http.Dir(path))
	fs = http.StripPrefix(endpoint, fs)

	router.PathPrefix(endpoint).Handler(fs)

	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      router,
		Addr:         config.Config.HTTPHost,
	}

	log.Info("Starting HTTP Server on: ", srv.Addr)

	if err := srv.ListenAndServe(); err != nil {
		log.Error("Failed to start HTTP server: ", err)
		return err
	}

	return nil
}
