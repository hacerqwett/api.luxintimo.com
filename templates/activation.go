package templates

// ActivateUserEmailTemplate Template for user activation mail
const ActivateUserEmailTemplate = `Здравей, {{.Name}}!

Ако искаш да завършиш процеса на регистрация, кликни на линка по-долу.

{{.ActivationLink}}`
