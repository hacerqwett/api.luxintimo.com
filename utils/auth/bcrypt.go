package auth

import (
	"encoding/base64"

	"api.luxintimo.com/log"
	"api.luxintimo.com/rerr"
	"golang.org/x/crypto/bcrypt"
)

const passwordComplexity = 14

// BCryptPassword Crypt a password
func BCryptPassword(password string) (string, *rerr.RestError) {
	sh, err := bcrypt.GenerateFromPassword([]byte(password), passwordComplexity)
	if err != nil {
		log.Error("Couldn't generate hash: ", err)
		return "", rerr.InternalServerError
	}
	pass := base64.StdEncoding.EncodeToString(sh)
	return pass, nil
}

// BCryptValidatePassword - Validates password against hash, nil if match, rerr.RestError if not
func BCryptValidatePassword(hash string, password string) *rerr.RestError {
	pass, decErr := base64.StdEncoding.DecodeString(hash)
	if decErr != nil {
		log.Error("Couldn't decode hash: ", decErr)
		return rerr.InternalServerError
	}

	compErr := bcrypt.CompareHashAndPassword(pass, []byte(password))
	if compErr != nil {
		log.Warning("Login failed, invalid password")
		return rerr.InvalidEmailOrPassword
	}

	return nil
}
