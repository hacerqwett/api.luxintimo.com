package auth

import (
	"net/http"
	"strings"

	"api.luxintimo.com/config"
	"api.luxintimo.com/log"
	"api.luxintimo.com/rerr"
	"api.luxintimo.com/utils/utime"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
)

// JWTGet - Gets JWT from Header, returns rerr.InvalidToken if invalid
func JWTGet(r *http.Request) (jwt.JWT, *rerr.RestError) {
	auth := r.Header.Get("Authorization")
	if auth == "" {
		log.Warning("No Authorization Header provided")
		return nil, rerr.InvalidToken
	}

	parts := strings.Split(auth, "Bearer ")
	if len(parts) < 2 {
		log.Warning("Invalid Authorization Header provided: ", auth)
		return nil, rerr.InvalidToken
	}

	tokenStr := parts[1]
	jwt, err := jws.ParseJWT([]byte(tokenStr))
	if err != nil {
		log.Warning("Invalid JWT provided: ", err, tokenStr)
		return nil, rerr.InvalidToken
	}

	return jwt, nil
}

// JWTCreate - Creates and Returns JWT from id
func JWTCreate(id int64) jwt.JWT {
	claims := jws.Claims{}
	claims.SetExpiration(utime.GetJWTExpirationTime())
	claims.Set("uid", id)

	return jws.NewJWT(claims, crypto.SigningMethodRS256)
}

// JWTValidate - Returns nil if valid, rerr.InvalidToken if invalid
func JWTValidate(token jwt.JWT) *rerr.RestError {
	if err := token.Validate(config.Config.RSAPublicKey, crypto.SigningMethodRS256); err != nil {
		log.Warning("JWT Validation Failed: ", err)
		return rerr.InvalidToken
	}

	return nil
}

// JWTValidateHeader - Returns nil if valid, rerr.InvalidToken if invalid
func JWTValidateHeader(r *http.Request) *rerr.RestError {
	j, err := JWTGet(r)
	if err != nil {
		return err
	}

	if err := JWTValidate(j); err != nil {
		return err
	}

	return nil
}

// JWTString - Converts JWT to string, returns rerr.InternalServerError if invalid
func JWTString(token jwt.JWT) (string, *rerr.RestError) {
	b, err := token.Serialize(config.Config.RSAPrivateKey)
	if err != nil {
		log.Error("Failed to Convert JWT to String: ", err)
		return "", rerr.InternalServerError
	}

	return string(b), nil
}

// JWTGetID - Gets id from JWT, returns rerr.RestError
func JWTGetID(token jwt.JWT) (int64, *rerr.RestError) {
	id := int64(token.Claims().Get("uid").(float64))
	if id < 1 {
		log.Warning("Invalid JWT ID: ", id)
		return -1, rerr.InvalidToken
	}
	return id, nil
}

// JWTGetIDFromHeader - Gets id from JWT, returns rerr.RestError
func JWTGetIDFromHeader(r *http.Request) (int64, *rerr.RestError) {
	j, err := JWTGet(r)
	if err != nil {
		return -1, err
	}

	id, err := JWTGetID(j)
	if err != nil {
		return -1, err
	}

	return id, nil
}
