package auth

import (
	"net/http"
	"strconv"
	"strings"

	"api.luxintimo.com/log"
	"api.luxintimo.com/rerr"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

// GenerateUUID Generate Random UUID
func GenerateUUID() (string, *rerr.RestError) {
	uuid, err := uuid.NewV4()
	if err != nil {
		log.Error("Failed to generate uuid: ", err)
		return "", rerr.InternalServerError
	}
	key := strings.Replace(uuid.String(), "-", "", -1)
	return key, nil
}

// GetValidUUID - Get UUID from request
func GetValidUUID(r *http.Request) (string, *rerr.RestError) {
	key, ok := mux.Vars(r)["key"]
	if !ok {
		log.Warning("Request Key not found")
		return "", rerr.UnknownRequestStructure
	}

	u, err := uuid.FromString(key)
	if err != nil {
		log.Warning("Invalid Request Key: ", key)
		return "", rerr.UnknownRequestStructure
	}

	return u.String(), nil
}

// GetValidID - Get ID from request
func GetValidID(r *http.Request) (int64, *rerr.RestError) {
	sid, ok := mux.Vars(r)["id"]
	if !ok {
		log.Warning("Request ID not found")
		return -1, rerr.UnknownRequestStructure
	}

	id, err := strconv.ParseInt(sid, 10, 64)
	if err != nil {
		log.Warning("Request ID not a number: ", sid)
		return -1, rerr.UnknownRequestStructure
	}

	if id < 1 {
		log.Warning("Request ID less than 1: ", sid)
		return -1, rerr.NotFound
	}

	return id, nil
}

// GetValidIndex - Get ID from request
func GetValidIndex(r *http.Request) (int64, *rerr.RestError) {
	sindex, ok := mux.Vars(r)["index"]
	if !ok {
		log.Warning("Request Index not found")
		return -1, rerr.UnknownRequestStructure
	}

	index, err := strconv.ParseInt(sindex, 10, 64)
	if err != nil {
		log.Warning("Request Index not a number: ", sindex)
		return -1, rerr.UnknownRequestStructure
	}

	if index < 0 {
		log.Warning("Request Index less than 0: ", sindex)
		return -1, rerr.NotFound
	}

	return index, nil
}
