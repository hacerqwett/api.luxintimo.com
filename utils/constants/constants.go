package constants

import (
	"api.luxintimo.com/utils/utils"
)

const (
	// GenderMale GenderMale
	GenderMale = iota + 1
	// GenderFemale GenderFemale
	GenderFemale
)

// ValidGender - Validate if g is valid gender
func ValidGender(g int64) bool {
	return g == GenderMale || g == GenderFemale
}

const (
	// SizeExtraSmall SizeExtraSmall
	SizeExtraSmall = iota + 1
	// SizeSmall SizeSmall
	SizeSmall
	// SizeMedium SizeMedium
	SizeMedium
	// SizeLarge SizeLarge
	SizeLarge
	// SizeExtraLarge SizeExtraLarge
	SizeExtraLarge
	// Size2ExtraLarge Size2ExtraLarge
	Size2ExtraLarge
	// Size3ExtraLarge Size3ExtraLarge
	Size3ExtraLarge
)

// ValidSize - Validate if s is valid size
func ValidSize(s int64) bool {
	return s >= SizeExtraSmall && s <= Size3ExtraLarge
}

const (
	// MimeTypeGIF MimeTypeGIF
	MimeTypeGIF = iota + 1
	// MimeTypePNG MimeTypePNG
	MimeTypePNG
	// MimeTypeJPEG MimeTypeJPEG
	MimeTypeJPEG
)

// MIMETypes MIMETypes
var MIMETypes = []string{
	MimeTypeGIF:  "image/gif",
	MimeTypePNG:  "image/png",
	MimeTypeJPEG: "image/jpeg",
}

// MIMETypesMap MIMETypesMap
var MIMETypesMap = utils.ArrToMap(MIMETypes)

// ValidMIMEType - Validate if m is valid MIME Type
func ValidMIMEType(m string) bool {
	_, ok := MIMETypesMap[m]
	return ok
}

// ValidHEX - Validates HEX Color Code
func ValidHEX(hex string) bool {
	l := len(hex)
	if l != 7 {
		return false
	}

	if hex[0] != '#' {
		return false
	}

	for _, c := range hex[1:] {
		if !(c >= 'A' && c <= 'F') && !(c >= 'a' && c <= 'f') && !(c >= '0' && c <= '9') {
			return false
		}
	}

	return true
}

const (
	// StatusDraft StatusDraft
	StatusDraft = iota + 1
	// StatusLive StatusLive
	StatusLive
	// StatusArchived StatusArchived
	StatusArchived
)

// ValidStatus - Validate if s is valid status
func ValidStatus(s int64) bool {
	return s >= StatusDraft && s <= StatusArchived
}

const (
	// AspectSquare AspectSquare
	AspectSquare = iota + 1
	// AspectRect75 AspectRect75
	AspectRect75
)

// ValidAspect - Validate if s is valid status
func ValidAspect(s int64) bool {
	return s >= AspectSquare && s <= AspectRect75
}
