package email

import (
	"api.luxintimo.com/config"
	"api.luxintimo.com/log"
	"api.luxintimo.com/rerr"
	gomail "gopkg.in/gomail.v2"
)

// SendActivationEmail - Sends Email with Activation URL to user
func SendActivationEmail(email string, key string) *rerr.RestError {
	activationURL := config.Config.ActivationURL + key
	emailErr := SendEmail(email, "Activate your Account", activationURL)
	if emailErr != nil {
		log.Error("Error sending activation email: ", emailErr)
		return rerr.InternalServerError
	}

	log.Notice("Sent activation email: ", activationURL)

	return nil
}

// SendEmail Send an email
func SendEmail(to string, subject string, content string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", config.Config.NoReplySender)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", content)
	d := gomail.NewDialer(config.Config.SMTPHost, config.Config.SMTPPort, config.Config.SMTPUsername, config.Config.SMTPPassword)
	return d.DialAndSend(m)
}
