package file

import (
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"

	"api.luxintimo.com/db"

	"api.luxintimo.com/utils/auth"

	"api.luxintimo.com/models"

	"api.luxintimo.com/log"

	"api.luxintimo.com/utils/constants"

	"api.luxintimo.com/config"
	"api.luxintimo.com/rerr"
)

const (
	// MB MB
	MB = 1 << 20
)

// ValidateSize - Validates Request size
func ValidateSize(w http.ResponseWriter, r *http.Request) *rerr.RestError {
	maxSize := config.Config.MaxFileSize * MB
	r.Body = http.MaxBytesReader(w, r.Body, maxSize)
	if err := r.ParseMultipartForm(maxSize); err != nil {
		log.Warning("File size exceeded: ", err)
		return rerr.ExceedMaxFileSize
	}

	return nil
}

// GetRequestData - Gets Request Data as String
func GetRequestData(r *http.Request) (string, *rerr.RestError) {
	val, err := GetValueFromRequest(r, "data")
	if err != nil {
		return "", err
	}

	return val, nil
}

// CreateFromRequest - Creates file from request, returns models.File
func CreateFromRequest(r *http.Request) (*models.File, *rerr.RestError) {
	file, header, err := GetFileFromRequest(r, config.Config.FormFileKey)
	if err != nil {
		return nil, err
	}

	b, err := GetBytes(file)
	if err != nil {
		return nil, err
	}

	typ, err := GetValidMIMEType(b)
	if err != nil {
		return nil, err
	}

	ext, err := GetExtension(typ)
	if err != nil {
		return nil, err
	}

	uuid, err := auth.GenerateUUID()
	if err != nil {
		return nil, err
	}

	name := uuid + ext
	path := BuildPath(name)

	if err := Create(b, path); err != nil {
		return nil, err
	}

	f := &models.File{
		Name:         db.NewNullString(name),
		OriginalName: db.NewNullString(header.Filename),
		Size:         db.NewNullInt64(header.Size),
		MIME:         db.NewNullInt64(constants.MIMETypesMap[typ]),
	}

	return f, nil
}

// GetValidMIMEType - Gets and validates MIME Type, returns it as string
func GetValidMIMEType(b []byte) (string, *rerr.RestError) {
	typ := http.DetectContentType(b)

	if !constants.ValidMIMEType(typ) {
		log.Warning("Invalid MIME Type: ", typ)
		return "", rerr.InvalidMIMEType
	}

	return typ, nil
}

// GetFileFromRequest - Gets file from request
func GetFileFromRequest(r *http.Request, key string) (multipart.File, *multipart.FileHeader, *rerr.RestError) {
	file, header, err := r.FormFile(key)
	if err != nil {
		log.Warning("Failed to form file from key: ", key, err)
		return nil, nil, rerr.UnknownRequestStructure
	}
	defer file.Close()

	return file, header, nil
}

// GetValueFromRequest - Gets value from request
func GetValueFromRequest(r *http.Request, key string) (string, *rerr.RestError) {
	val := r.FormValue(key)
	if val == "" {
		log.Warning("Failed to get form value from key: ", key)
		return "", rerr.UnknownRequestStructure
	}

	return val, nil
}

// GetBytes - Returns bytes
func GetBytes(f io.Reader) ([]byte, *rerr.RestError) {
	b, err := ioutil.ReadAll(f)
	if err != nil {
		log.Warning("Failed to read file: ", err)
		return nil, rerr.InternalServerError
	}

	return b, nil
}

// GetExtension - Gets extension type from mime
func GetExtension(m string) (string, *rerr.RestError) {
	ext, err := mime.ExtensionsByType(m)
	if err != nil || ext == nil {
		log.Warning("Failed to find extension by type: ", m, err)
		return "", rerr.InvalidMIMEType
	}

	return ext[0], nil
}

// BuildPath - Builds file path
func BuildPath(name string) string {
	return filepath.Join(config.Config.StaticFilesPath, name)
}

// Create - Creates file in OS, returns rerr.RestError
func Create(b []byte, path string) *rerr.RestError {
	if err := ioutil.WriteFile(path, b, 0666); err != nil {
		log.Error("Failed to create file: ", err)
		return rerr.InternalServerError
	}

	return nil
}

// Delete - Deletes file in OS, returns rerr.RestError if failed
func Delete(path string) *rerr.RestError {
	files, err := filepath.Glob(path)
	if err != nil {
		log.Error("Couldn't find filepath matches: ", path, err)
		return rerr.InternalServerError
	}

	for _, f := range files {
		if err := os.Remove(f); err != nil {
			log.Error("Failed to remove file: ", err)
			return rerr.InternalServerError
		}
	}

	return nil
}

// CompressImage - Creates copies, resizes, compresses images
func CompressImage(name string, aspect int64) *rerr.RestError {
	ex, err := os.Executable()
	if err != nil {
		log.Error("Failed to get executable path: ", err)
		return rerr.InternalServerError
	}

	exPath := filepath.Dir(ex)
	cmd := exec.Command(exPath+"/scripts/shell/compress_image.sh", name, strconv.FormatInt(aspect, 10))
	cmd.Dir = exPath + "/static"

	if err := cmd.Run(); err != nil {
		log.Error("Failed to compress image: ", err)
		return rerr.InternalServerError
	}

	return nil
}
