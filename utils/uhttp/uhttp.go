package uhttp

import (
	"encoding/json"
	"net/http"

	"api.luxintimo.com/log"
	"api.luxintimo.com/rerr"
)

// ReadJSON - Reads JSON from Request,
func ReadJSON(r *http.Request, v interface{}) *rerr.RestError {
	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		log.Warning("Unknown request structure:", err)
		return rerr.UnknownRequestStructure
	}
	return nil
}

// WriteJSON - Writes JSON as http Response.
func WriteJSON(w http.ResponseWriter, v interface{}) {
	js, err := json.Marshal(v)
	if err != nil {
		log.Error("Failed to Marshal JSON", err)
		WriteError(w, rerr.InternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	_, err = w.Write(js)
	if err != nil {
		log.Error("Failed to write JSON", err)
		WriteError(w, rerr.InternalServerError)
	}
}

// WriteOK - Sends back code 200
func WriteOK(w http.ResponseWriter) {
	WriteJSON(w, struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}{200, "ok"})
}

// WriteError - Writes error as http Response.
func WriteError(w http.ResponseWriter, e *rerr.RestError) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.Code)
	_, err := w.Write(e.ToJSON())
	if err != nil {
		log.Error("Failed to write Error", err)
	}
}
