package utils

// ContainsInt - ContainsInt
func ContainsInt(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// ContainsString - ContainsString
func ContainsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// IndexOfStringArr - IndexOfStringArr
func IndexOfStringArr(s []string, e string) int {
	for i, a := range s {
		if a == e {
			return i
		}
	}
	return -1
}

// ArrToMap - Converts string array to map
func ArrToMap(arr []string) map[string]int {
	res := make(map[string]int)
	for i, el := range arr {
		res[el] = i
	}
	return res
}
