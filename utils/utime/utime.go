package utime

import (
	"time"

	"api.luxintimo.com/config"
)

// GetTime Gets current UTC time
func GetTime() time.Time {
	return time.Now().UTC()
}

// GetJWTExpirationTime - Gets JWT Expiration Time
func GetJWTExpirationTime() time.Time {
	curr := GetTime()
	h := time.Duration(config.Config.JWTExpirationHours) * time.Hour
	exp := curr.Add(h)

	return exp
}
